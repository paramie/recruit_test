class GameSettingData {
  String matchID;
  String matchName;
  String player1;
  String player2;
  String noOfGames;
  String noOfSets;
  String wheather;
  String advantage;
  String tieBreak;
  String stadium;
  String courtType;
  String courtNumber;
  String gameDate;
  String gameTime;

  GameSettingData(
      {this.matchID,
      this.matchName,
      this.player1,
      this.player2,
      this.noOfGames,
      this.noOfSets,
      this.wheather,
      this.advantage,
      this.tieBreak,
      this.stadium,
      this.courtType,
      this.courtNumber,
      this.gameDate,
      this.gameTime});
}

class PlayerData {
  String playerId;
  String name;
  String gender;
  String dateOfBirth;
  String handedness;
  String playstyle;
  String team;
  String role;
  String playerImage;

  PlayerData(
      {this.playerId,
      this.name,
      this.gender,
      this.dateOfBirth,
      this.handedness,
      this.playstyle,
      this.team,
      this.role,
      this.playerImage});
}

class ResultData {
  String matchId;
  String gameTime;
  String winner;
  String looser;
  String winnerSet;
  String looserSet;
  int gameStatus;
  int gameTotTime;

  ResultData(
      {this.matchId,
      this.gameTime,
      this.winner,
      this.looser,
      this.winnerSet,
      this.looserSet,
      this.gameStatus,
      this.gameTotTime});
}

class SetInfotData {
  String matchId;
  String player1Id;
  String player2Id;
  String p1Set1;
  String p1Set2;
  String p1Set3;
  String p1Set4;
  String p1Set5;
  String p2Set1;
  String p2Set2;
  String p2Set3;
  String p2Set4;
  String p2Set5;

  SetInfotData(
      {this.matchId,
      this.player1Id,
      this.player2Id,
      this.p1Set1,
      this.p1Set2,
      this.p1Set3,
      this.p1Set4,
      this.p1Set5,
      this.p2Set1,
      this.p2Set2,
      this.p2Set3,
      this.p2Set4,
      this.p2Set5});
}

class ScoreData {
  String matchId;
  String won;
  int serve;
  int player1Sets;
  int player1Games;
  int player1Points;
  int player2Sets;
  int player2Games;
  int player2Points;
  int totalSets;
  int totalGames;
  int setFinished;
  int tieBreakOn;
  int gameTime;

  ScoreData({
    this.matchId,
    this.won,
    this.serve,
    this.player1Sets,
    this.player1Games,
    this.player1Points,
    this.player2Sets,
    this.player2Games,
    this.player2Points,
    this.totalSets,
    this.totalGames,
    this.setFinished,
    this.tieBreakOn,
    this.gameTime,
  });
}

class ScoreDataAll {
  int id;
  String matchId;
  String won;
  int serve;
  int player1Sets;
  int player1Games;
  int player1Points;
  int player2Sets;
  int player2Games;
  int player2Points;
  int totalSets;
  int totalGames;
  int setFinished;

  ScoreDataAll({
    this.id,
    this.matchId,
    this.won,
    this.serve,
    this.player1Sets,
    this.player1Games,
    this.player1Points,
    this.player2Sets,
    this.player2Games,
    this.player2Points,
    this.totalSets,
    this.totalGames,
    this.setFinished,
  });
}
