class Stadium {
  String stadiumId;
  String stadiumName;
  String stadiumAddress;
  String contactNumber;
  int noOfCourts;
  String reservationUrl;
  String stadiumImage;
  double stadiumLat;
  double stadiumLon;
  int status;

  Stadium(
      {this.stadiumId,
      this.stadiumName,
      this.stadiumAddress,
      this.contactNumber,
      this.noOfCourts,
      this.reservationUrl,
      this.stadiumImage,
      this.stadiumLat,
      this.stadiumLon,
      this.status});
}

class Court {
  String courtId;
  String stadiumId;
  String courtName;
  String serface;
  String indoor;
  String availableTime;
  String stadiumName;
  String courtImage;
  String status;

  Court(
      {this.courtId,
      this.stadiumId,
      this.courtName,
      this.serface,
      this.indoor,
      this.availableTime,
      this.stadiumName,
      this.courtImage,
      this.status});
}
