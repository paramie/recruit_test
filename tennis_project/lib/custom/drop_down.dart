import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';

class DrpDown extends StatefulWidget {
  DrpDown({
    Key key,
    this.topic,
    this.values,
    this.onChanged,
    this.selectedVal,
    this.child,
    this.borderColor,
    this.errorText,
    // this.color = const Color(0xFFFFE306),
    // this.child,
  }) : super(key: key);

  final Widget child;
  final String selectedVal;
  final values;
  final String topic;
  final Function onChanged;
  final borderColor;
  final String errorText;
  // final Color color;
  // final Widget child;

  @override
  State<StatefulWidget> createState() => _DrpDown();
}

class _DrpDown extends State<DrpDown> {
  String dropdownValue = '';
  @override
  void initState() {
    super.initState();
    dropdownValue = null;
  }

  static const form_width = 280.0;
  static double rowHeight = 80.0;
  static const box_height = 48.0;
  static const default_font_size = AppStyles.normal_font;
  static const default_padding = 10.0;
  static const text_box_height = (box_height - default_font_size) / 2.0;

  @override
  Widget build(BuildContext context) {
    var boderColor;
    var errorTextContainer;

    if (widget.borderColor == null) {
      boderColor = AppColors.form_border;
    } else {
      boderColor = widget.borderColor;
    }
    if (widget.errorText == null) {
      setState(() {
        rowHeight = 80.0;
      });
      errorTextContainer = Container(
        width: 300.0,
        height: 5,
      );
    } else {
      setState(() {
        rowHeight = 100.0;
      });
      errorTextContainer = Container(
        width: 300.0,
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: new Text(
            widget.errorText,
            style: TextStyle(
              color: Colors.red,
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      );
    }

    return Align(
      child: Container(
        width: form_width,
        height: rowHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                child: Flexible(
              child: Text(
                widget.topic,
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: default_font_size,
                    color: AppColors.black,
                    fontWeight: FontWeight.w600),
              ),
            )),
            Container(
                child: Container(
              width: form_width,
              height: box_height,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                  color: boderColor,
                  style: BorderStyle.solid,
                  width: 1.2,
                ),
              ),
              child: Stack(
                children: <Widget>[
                  Positioned(
                    right: 0,
                    top: 0,
                    child: Container(
                      width: box_height,
                      height: box_height,
                      child: Center(
                        child: Icon(Icons.expand_more,
                            size: 24, color: Colors.black),
                      ),
                    ),
                  ),
                  Theme(
                    data: Theme.of(context).copyWith(),
                    child: DropdownButtonHideUnderline(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(
                            default_padding, 0.0, default_padding, 0.0),
                        child: widget.child,
                      ),
                    ),
                  ),
                ],
              ),
            )),
            errorTextContainer,
          ],
        ),
      ),
    );
  }
}
