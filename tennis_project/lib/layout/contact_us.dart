import 'dart:ui';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tenizo/util/app_const.dart';

class ContactUs extends StatefulWidget {
  final arguments;
  final Function onSelected;

  @override
  _ContactUsState createState() => _ContactUsState();
  ContactUs({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _ContactUsState extends State<ContactUs> with BaseControllerListner {
  BaseController controller;

  @override
  void initState() {
    super.initState();
    controller = new BaseController(this);
  }

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;
  var paddingData = const EdgeInsets.only(left: 210.0, top: 120.0);

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      paddingData = const EdgeInsets.only(left: 190.0, top: 130.0);
    } else if (deviceWidth <= 400) {
      paddingData = const EdgeInsets.only(left: 200.0, top: 130.0);
    } else if (deviceWidth <= 450) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 500) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 550) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else {}
  }

  double paraSize = 15.0;
  static const copyRightColor = const Color(0x8A000000);

  paragraphStyle() { 
    TextStyle(
        fontSize: paraSize,
        fontWeight: FontWeight.w600,
        fontFamily: AppStyles.normal_font.toString());
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MediaQuery(
            data: MediaQueryData(), 
            child: Text('Contact Us',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700))),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: MediaQuery(
          data: MediaQueryData(),
          child: SingleChildScrollView(
            //main body container
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: Column(
                  children: <Widget>[
                    //title
                    Container(
                      width: MediaQuery.of(context).size.width,
                      // decoration: BoxDecoration(color: Colors.red),
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          "Tennis Score Management",
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w600,
                            
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),

                    //subtitle
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Text(
                          "Application",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),

                    //paragraph
                    Container(
                      width: MediaQuery.of(context).size.width,
                      //decoration: BoxDecoration(color: Colors.red),
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          "A form of tennis scoring application. Can use to coaches, parents and any. Easy & Simple...",
                          style: paragraphStyle(),
                        ),
                      ),
                    ),


                    //contact us
                    Container(
                      width: MediaQuery.of(context).size.width,
                      //decoration: BoxDecoration(color: Colors.red),
                      child: Padding(
                          padding: EdgeInsets.only(top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: 100.0,
                                child: Text(
                                  "Contact Us:",
                                  style: paragraphStyle(),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "TEL : +81 11-215-8072",
                                        style: paragraphStyle(),
                                        textAlign: TextAlign.left,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top :5.0),
                                        child: Text(
                                          "FAX : +81 11-215-8072",
                                          style: paragraphStyle(),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20.0,
                                      ),
                                      Text(
                                        "NY Bld.2F, 13-1-22, Hongodori,",
                                        style: paragraphStyle(),
                                        textAlign: TextAlign.left,
                                      ),
                                      Text(
                                        "Shiroishi Ku, Sapporo Shi, Hokkaido, Japan.",
                                        style: paragraphStyle(),
                                        textAlign: TextAlign.left,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top:5.0),
                                        child: Text(
                                          "003-0024",
                                          style: paragraphStyle(),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20.0,
                                      ),
                                      Text(
                                        "info@arit-clt.com",
                                        style: paragraphStyle(),
                                        textAlign: TextAlign.left,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ),


                    SizedBox(
                      height: 50,
                    ),

                    //copyRight
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 0.0),
                        child: Column(
                          children: <Widget>[
                            Center(
                              child: Text(
                                "2019 All Rights Reserved",
                                style: TextStyle(
                                  fontSize: 13,
                                  color: copyRightColor,
                                ),
                              ),
                            ),
                            Center(
                              child: Text(
                                "TenniZo is a registered trade mark of ARIT Co., LTD.",
                                style: TextStyle(
                                  fontSize: 13,
                                  color: copyRightColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {}
}
