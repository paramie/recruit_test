import 'package:tenizo/const.dart';
import 'package:tenizo/util/app_rallyChanger.dart';
import 'package:tenizo/util/app_selectbox.dart';
// import 'package:tenizo/controller/game_setting_data.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/util/app_const.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tenizo/util/chart.dart';

import '../tennizo_controller_functions.dart';

List<Pointer> scoreValues;

class FlowChart extends StatefulWidget {
  final Function onSelected;
  final Function onArgumentSaving;
  final arguments;
  final initAd;
  final showAd;

  @override
  _FlowChartState createState() => _FlowChartState();
  FlowChart(
      {Key key,
      this.onSelected,
      this.onArgumentSaving,
      this.arguments,
      this.initAd,
      this.showAd})
      : super(key: key);
}

class _FlowChartState extends State<FlowChart> with BaseControllerListner {
  BaseController controller;
  double scaler = 22;
  double dataSpreadFactorA = 10;
  double dataSpreadFactorB = 2;
  double noOfElement = 20;
  double dataSpreadXFactor;
  double dataSpreadYFactor;
  double graphInnerBoxXoffsetFactorLeft = 1;
  double graphInnerBoxXoffsetFactorRight = 1;
  double graphInnerBoxYoffsetFactorTop = 3;
  double graphInnerBoxYoffsetFactorBottom = 2;
  double sheetWidthFactor;
  double sheetHightFactor;
  double finalWidthOFSHEET;
  double finalHightOFSHEET;
  ScrollController scrollControllerX = ScrollController();
  ScrollController scrollControllerY = ScrollController();
  GlobalKey _keyRed;
  double beforeCenterX = 0;
  double beforeCenterY = 0;
  double afterCenterX = 0;
  double afterCenterY = 0;
  double onlyScaleUpdateX = 0;
  double onlyScaleUpdateY = 0;
  String player1Name = "";
  String player2Name = "";
  bool upArrowShow = false;
  bool downArrowShow = false;
  double upArrowPositionX = 0;
  double upArrowPositionY = 0;
  double arrowSize = 30;
  double downArrowPositionX = 0;
  double downArrowPositionY = 0;
  bool settingShow = false;
  double settingSize = 26;
  double settingPositionX = 0;
  double settingPositionY = 0;
  int selectedIndex = 0;
  double chartKeySizeBoxWidth = 50.0;
  double chartkeyFontSize = 20.0;
  double playerNameFontSize = 25.0;

  double focusPosX;
  double focusPosY;
  Widget upArrow;
  Widget settings;
  Widget downArrow;
  double testPosX = 0;
  double testPosY = 0;
  double dotSize = 18;
  int dropdownTypeValue = 1;
  int dropdownServiceValue = 1;
  int dropdownRallyCount = 1;
  int itemNeedToBePassed = 0;
  int bigPointWinner = 1;
  static var deviceWidth = 0.0;
  List<int> point = [0, 0];


  List arrayType = [
    TypeClass('Ace', Color(0xFFDB2828), 1),
    TypeClass('Double Fault', Color(0xFFFBBD08), 2),
    TypeClass('Winner', Color(0xFF00B5AD), 3),
    TypeClass('Winner-BackHand', Color(0xFF00B5AD), 4),
    TypeClass('Winner-BackHandVolley', Color(0xFF00B5AD), 5),
    TypeClass('Winner-ForeHand', Color(0xFF00B5AD), 6),
    TypeClass('Winner-ForeHandVolley', Color(0xFF00B5AD), 7),
    TypeClass('Winner-Smash', Color(0xFF00B5AD), 8),
    TypeClass('u.Error', Color(0xFF000000), 9),
    TypeClass('u.Error-BackHand', Color(0xFF000000), 10),
    TypeClass('u.Error-BackHandVolley', Color(0xFF000000), 11),
    TypeClass('u.Error-ForeHand', Color(0xFF000000), 12),
    TypeClass('u.Error-ForeHandVolley', Color(0xFF000000), 13),
    TypeClass('u.Error-Smash', Color(0xFF000000), 14),
    TypeClass('f.Error', Color(0xFF0931FC), 15),
    TypeClass('f.Error-BackHand', Color(0xFF0931FC), 16),
    TypeClass('f.Error-BackHandVolley', Color(0xFF0931FC), 17),
    TypeClass('f.Error-ForeHand', Color(0xFF0931FC), 18),
    TypeClass('f.Error-ForeHandVolley', Color(0xFF0931FC), 19),
    TypeClass('f.Error-Smash', Color(0xFF0931FC), 20),
  ];

  bool whatIsTrue = false;
  int enabledItem = 0;

  List arrayService = [
    TypeClass('First Service', Color(0xFFA333C8), 1),
    TypeClass('Second Service', Color(0xFFF2711C), 2),
  ];

  List<List<int>> points;

  @override
  void initState() {
    super.initState();

    // if (Const.buttonCountflow == 1) {
    //   widget.showAd(0);
    // }

    controller = new BaseController(this);
    dataSpreadXFactor = noOfElement;
    dataSpreadYFactor = dataSpreadFactorA + dataSpreadFactorB;
    sheetWidthFactor = dataSpreadXFactor +
        graphInnerBoxXoffsetFactorLeft +
        graphInnerBoxXoffsetFactorRight;
    sheetHightFactor = dataSpreadYFactor +
        graphInnerBoxYoffsetFactorTop +
        graphInnerBoxYoffsetFactorBottom;
    finalWidthOFSHEET = sheetWidthFactor * scaler;
    finalHightOFSHEET = sheetHightFactor * scaler;
    _keyRed = GlobalKey();
    // print("match id" + widget.arguments["matchID"]);
    getSocreData();
    upArrow = SvgPicture.asset('images/up-arrow.svg');
    downArrow = SvgPicture.asset('images/down-arrow.svg');
    settings = SvgPicture.asset('images/settings.svg');
  }

  void getSocreData() {
    var selectQuery = [
      "SELECT * FROM scoreData where match_id = ?",
      [widget.arguments["matchID"]],
      {"calledMethod": "getSocreData"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  void playerUPDOWNChange() {
    var wonID;
    if (bigPointWinner < 0) {
      wonID = widget.arguments["player1"];
    } else {
      wonID = widget.arguments["player2"];
    }
    var updateQuery = [
      "UPDATE scoreData SET won=? WHERE id=?",
      [
        wonID,
        scoreValues[selectedIndex].id,
      ],
      {"calledMethod": "playerUPDOWNChange"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_update, updateQuery);
  }

  void changTheGraphDataOnTheFly(tappedOn) {
    if (tappedOn == 'up') {
      setState(() {
        scoreValues[selectedIndex].pointWinner = 1;
        downArrowShow = false;
        upArrowShow = false;
        settingShow = false;
        dataSpreadFactorA = dataSpreadFactorA + 2;
        dataSpreadYFactor = dataSpreadFactorA + dataSpreadFactorB;
        sheetWidthFactor = dataSpreadXFactor +
            graphInnerBoxXoffsetFactorLeft +
            graphInnerBoxXoffsetFactorRight;
        sheetHightFactor = dataSpreadYFactor +
            graphInnerBoxYoffsetFactorTop +
            graphInnerBoxYoffsetFactorBottom;
        finalWidthOFSHEET = sheetWidthFactor * scaler;
        finalHightOFSHEET = sheetHightFactor * scaler;
      });
      playerUPDOWNChange();
    } else {
      setState(() {
        scoreValues[selectedIndex].pointWinner = -1;
        upArrowShow = false;
        downArrowShow = false;
        settingShow = false;
        dataSpreadFactorB = dataSpreadFactorB + 2;
        dataSpreadYFactor = dataSpreadFactorA + dataSpreadFactorB;
        sheetWidthFactor = dataSpreadXFactor +
            graphInnerBoxXoffsetFactorLeft +
            graphInnerBoxXoffsetFactorRight;
        sheetHightFactor = dataSpreadYFactor +
            graphInnerBoxYoffsetFactorTop +
            graphInnerBoxYoffsetFactorBottom;
        finalWidthOFSHEET = sheetWidthFactor * scaler;
        finalHightOFSHEET = sheetHightFactor * scaler;
      });
      playerUPDOWNChange();
    }
  }

  void changeSetttingsOnTheFly(context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            contentPadding: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
            content: SizedBox(
              height: 350,
              width: 320,
              child: SingleChildScrollView(
                child: Container(
                  // color: Colors.green,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Select the Type : ",
                              style: TextStyle(fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ),
                      SelectBox(
                        items: arrayType.map(((obj) {
                          return SelectBoxItem(obj.name, obj.type,
                              Icons.fiber_manual_record, obj.color);
                        })).toList(),
                        onChanged: (SelectBoxItem _item) {
                          //print(_item.type);
                          setState(() {
                            dropdownTypeValue = _item.type;
                            if (_item.type == 2) {
                              whatIsTrue = true;
                            } else {
                              whatIsTrue = false;
                            }
                          });
                        },
                        selectItm: dropdownTypeValue - 1,
                        enabledItem: enabledItem,
                        whatIsTrue: whatIsTrue,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Select the Serve : ",
                              style: TextStyle(fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ),
                      SelectBox(
                        items: arrayService.map(((obj) {
                          return SelectBoxItem(obj.name, obj.type,
                              Icons.fiber_manual_record, obj.color);
                        })).toList(),
                        onChanged: (SelectBoxItem _item) {
                          setState(() {
                            dropdownServiceValue = _item.type;
                          });
                        },
                        selectItm: dropdownServiceValue - 1,
                        enabledItem: enabledItem,
                        whatIsTrue: whatIsTrue,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Set the Rally Count : ",
                              style: TextStyle(fontWeight: FontWeight.w600),
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 25.0, bottom: 10.0),
                        child: RallyChanger(
                            dropdownRallyCount: dropdownRallyCount,
                            onIncremented: (val) {
                              setState(() {
                                dropdownRallyCount = val;
                              });
                            },
                            onDecremented: (val) {
                              setState(() {
                                dropdownRallyCount = val;
                              });
                            }),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            FlatButton(
                              child: Text('Save'),
                              onPressed: () {
                                Navigator.of(context).pop();
                                setState(() {
                                  settingShow = false;
                                  upArrowShow = false;
                                  downArrowShow = false;
                                });
                                for (var i = 0; i < scoreValues.length; i++) {
                                  if (scoreValues[i].id == itemNeedToBePassed) {
                                    setState(() {
                                      scoreValues[i].rallyCount =
                                          dropdownRallyCount;
                                      scoreValues[i].type = dropdownTypeValue;
                                      scoreValues[i].serviceChance =
                                          dropdownServiceValue;
                                    });

                                    var temVal;
                                    if (dropdownTypeValue == 1) {
                                      temVal = ScoreValues.ace;
                                    } else if (dropdownTypeValue == 2) {
                                      temVal = ScoreValues.dflt;
                                    } else if (dropdownTypeValue == 3) {
                                      temVal = ScoreValues.winner;
                                    } else if (dropdownTypeValue == 4) {
                                      temVal = ScoreValues.backHand;
                                    } else if (dropdownTypeValue == 5) {
                                      temVal = ScoreValues.backHandVolley;
                                    } else if (dropdownTypeValue == 6) {
                                      temVal = ScoreValues.foreHand;
                                    } else if (dropdownTypeValue == 7) {
                                      temVal = ScoreValues.forHandVolley;
                                    } else if (dropdownTypeValue == 8) {
                                      temVal = ScoreValues.smash;
                                    } else if (dropdownTypeValue == 9) {
                                      temVal = ScoreValues.uErr;
                                    } else if (dropdownTypeValue == 10) {
                                      temVal = ScoreValues.uErrBackHand;
                                    } else if (dropdownTypeValue == 11) {
                                      temVal = ScoreValues.uErrBackHandVolley;
                                    } else if (dropdownTypeValue == 12) {
                                      temVal = ScoreValues.uErrForeHand;
                                    } else if (dropdownTypeValue == 13) {
                                      temVal = ScoreValues.uErrForHandVolley;
                                    } else if (dropdownTypeValue == 14) {
                                      temVal = ScoreValues.uErrSmash;
                                    } else if (dropdownTypeValue == 15) {
                                      temVal = ScoreValues.fErr;
                                    } else if (dropdownTypeValue == 16) {
                                      temVal = ScoreValues.fErrBackHand;
                                    } else if (dropdownTypeValue == 17) {
                                      temVal = ScoreValues.fErrBackHandVolley;
                                    } else if (dropdownTypeValue == 18) {
                                      temVal = ScoreValues.fErrForeHand;
                                    } else if (dropdownTypeValue == 19) {
                                      temVal = ScoreValues.fErrForHandVolley;
                                    } else if (dropdownTypeValue == 20) {
                                      temVal = ScoreValues.fErrSmash;
                                    }

                                    // List queries = [];
                                    // List params = [];

                                    // queries.add(
                                    //     "UPDATE scoreData SET rally=?, type=? , serveChance=? WHERE id=?");
                                    // params.add([
                                    //   dropdownRallyCount,
                                    //   temVal,
                                    //   dropdownServiceValue,
                                    //   scoreValues[i].id,
                                    // ]);

                                    // var querywithParams = [
                                    //   queries,
                                    //   params,
                                    //   {"calledMethod": 'updateGraphData'}
                                    // ];

                                    // controller.execFunction(
                                    //     ControllerFunc.db_sqlite,
                                    //     ControllerSubFunc.db_select_batch,
                                    //     querywithParams);

                                    var updateQuery = [
                                      "UPDATE scoreData SET rally=?, type=? , serveChance=? WHERE id=?",
                                      [
                                        dropdownRallyCount,
                                        temVal,
                                        dropdownServiceValue,
                                        scoreValues[i].id,
                                      ],
                                      {"calledMethod": "updateGraphData"}
                                    ];
                                    controller.execFunction(
                                        ControllerFunc.db_sqlite,
                                        ControllerSubFunc.db_update,
                                        updateQuery);

                                    break;
                                  }
                                }
                              },
                              color: Colors.blue,
                              colorBrightness: Brightness.dark,
                              disabledColor: Colors.blueGrey,
                              highlightColor: Colors.red,
                              padding: EdgeInsets.symmetric(
                                horizontal: 8.0,
                                vertical: 5.0,
                              ),
                            ),
                            FlatButton(
                              child: Text('Cancel'),
                              onPressed: () {
                                Navigator.of(context).pop();
                                setState(() {
                                  settingShow = false;
                                  upArrowShow = false;
                                  downArrowShow = false;
                                });
                              },
                              color: Colors.blue,
                              colorBrightness: Brightness.dark,
                              disabledColor: Colors.blueGrey,
                              highlightColor: Colors.red,
                              padding: EdgeInsets.symmetric(
                                horizontal: 8.0,
                                vertical: 5.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // actions: <Widget>[
            //   Row(
            //     children: <Widget>[
            //       Container(
            //         width: ,
            //         color: Colors.yellow,
            //         child: GestureDetector(
            //           child: Text("Save"),
            //         ),
            //       ),
            //     ],
            //   )
            // ],
          );
        });
  }

  void setArrowPositionOnTheFly(
    double x,
    double y,
    int pointWinner,
    int type,
    int rallyCount,
    id,
    serviceChance,
  ) {
    setState(() {
      upArrowPositionX = x - (arrowSize / 2);
      upArrowPositionY = y - (arrowSize * 2.5);
      downArrowPositionX = x - (arrowSize / 2);
      downArrowPositionY = y + (arrowSize * 1.2);
      if (pointWinner > 0) {
        upArrowShow = false;
        downArrowShow = true;
      } else {
        upArrowShow = true;
        downArrowShow = false;
      }
      dropdownTypeValue = type;
      dropdownRallyCount = rallyCount;
      itemNeedToBePassed = id;
      dropdownServiceValue = serviceChance;
    });
    // Settings button position
    setState(() {
      settingShow = true;
      if (pointWinner > 0) {
        bigPointWinner = 1;
        settingPositionX = x - (settingSize / 2);
        settingPositionY = y - (settingSize * 2.5);
      } else {
        bigPointWinner = -1;
        settingPositionX = x - (settingSize / 2);
        settingPositionY = y + (settingSize * 1.2);
      }
    });
  }

  void getUserNames(id1, id2) {
    var selectQuery = [
      "SELECT * FROM player where player_id = ? or player_id = ?",
      [id1, id2],
      {"calledMethod": "getUserNames"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  _setDeviceWidth() {
    //print("deviceWidth "+deviceWidth.toString());
    if (deviceWidth <= 350) {
      chartKeySizeBoxWidth = 20.0;
      chartkeyFontSize = 15.0;
      playerNameFontSize = 20.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: Container(
          color: AppColors.secondary_color,
          child: Column(
            children: <Widget>[
              Container(
                height: 50,
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(
                        color: AppColors.black,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                            child: Container(
                              width: double.infinity / 2,
                              decoration: BoxDecoration(
                                color: AppColors.ternary_color,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5.0),
                                  bottomLeft: Radius.circular(5.0),
                                ),
                              ),
                              height: 50,
                              child: Center(
                                child: Text(
                                  "Flow",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                            onTap: () {
                              widget.onSelected(RoutingData.Flow, false, true);
                            },
                          ),
                        ),
                        Expanded(
                          child: GestureDetector(
                            child: Container(
                              width: double.infinity / 2,
                              height: 50,
                              decoration: BoxDecoration(
                                border: Border(
                                  left: BorderSide(
                                      width: 1.0, color: Colors.black),
                                ),
                              ),
                              child: Center(
                                child: Text(
                                  "Stats",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                            onTap: () {
                              widget.onArgumentSaving({
                                "matchID": widget.arguments["matchID"],
                                "player1": widget.arguments["player1"],
                                "player2": widget.arguments["player2"],
                                "time": widget.arguments["time"],
                              });
                              widget.onSelected(RoutingData.Stats, false, true);
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Column(
                children: <Widget>[
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    width: deviceWidth - 50,
                    height: 30,
                    // color: Colors.red,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          left: 1.0,
                          child: Container(
                            width: 50,
                            height: 30.0,
                            // color: Colors.teal,
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                point[0].toString(),
                                style: TextStyle(
                                    fontSize: playerNameFontSize,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            player1Name,
                            style: TextStyle(
                                fontSize: playerNameFontSize,
                                fontWeight: FontWeight.w600,
                                color: Colors.black),
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    key: _keyRed,
                    width: MediaQuery.of(context).size.width - 10,
                    height: MediaQuery.of(context).size.height - 350,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.black54),
                    ),
                    child: Scrollbar(
                      child: SingleChildScrollView(
                        controller: scrollControllerX,
                        scrollDirection: Axis.horizontal,
                        child: Container(
                          width: finalWidthOFSHEET,
                          child: SingleChildScrollView(
                            controller: scrollControllerY,
                            scrollDirection: Axis.vertical,
                            child: Container(
                              width: finalWidthOFSHEET,
                              height: finalHightOFSHEET,
                              child: GestureDetector(
                                onTapUp: (detail) {
                                  setState(() {
                                    upArrowShow = false;
                                    downArrowShow = false;
                                    settingShow = false;
                                  });
                                },
                                onLongPressStart: (detail) {
                                  double dotScale = scaler > 28 ? 1 : .8;
                                  for (var i = 0; i < scoreValues.length; i++) {
                                    if ((scoreValues[i].yTrueVal +
                                                (dotSize * dotScale) >=
                                            detail.localPosition.dy) &&
                                        (scoreValues[i].yTrueVal -
                                                (dotSize * dotScale) <=
                                            detail.localPosition.dy) &&
                                        (scoreValues[i].xTrueVal +
                                                (dotSize * dotScale) >=
                                            detail.localPosition.dx) &&
                                        (scoreValues[i].xTrueVal -
                                                (dotSize * dotScale) <=
                                            detail.localPosition.dx)) {
                                      setState(() {
                                        selectedIndex = i;
                                        testPosX = scoreValues[i].xTrueVal;
                                        testPosY = scoreValues[i].yTrueVal;
                                      });
                                      setArrowPositionOnTheFly(
                                          scoreValues[i].xTrueVal,
                                          scoreValues[i].yTrueVal,
                                          scoreValues[i].pointWinner,
                                          scoreValues[i].type,
                                          scoreValues[i].rallyCount,
                                          scoreValues[i].id,
                                          scoreValues[i].serviceChance);
                                      break;
                                    }
                                  }
                                },
                                onScaleStart: (scaleDetails) {
                                  setState(() {
                                    focusPosX = scaleDetails.localFocalPoint.dx;
                                    focusPosY = scaleDetails.localFocalPoint.dy;
                                    beforeCenterX =
                                        sheetWidthFactor * scaler / 2;
                                    beforeCenterY =
                                        sheetHightFactor * scaler / 2;
                                    upArrowShow = false;
                                    downArrowShow = false;
                                  });
                                },
                                onScaleEnd: (scaleDetails) {},
                                onScaleUpdate:
                                    (ScaleUpdateDetails scaleDetails) {
                                  setState(() {
                                    focusPosX = scaleDetails.localFocalPoint.dx;
                                    focusPosY = scaleDetails.localFocalPoint.dy;
                                    afterCenterX = 0;
                                    afterCenterY = 0;
                                  });

                                  setState(() {
                                    if (scaleDetails.scale > 1) {
                                      if (scaler <= 100) {
                                        scaler =
                                            scaler + scaleDetails.scale / 2;
                                      } else {
                                        // scaler = 300;
                                      }
                                      finalWidthOFSHEET =
                                          sheetWidthFactor * scaler;
                                      finalHightOFSHEET =
                                          (sheetHightFactor * scaler) + scaler;
                                      dotSize = (scaler) * 0.8;
                                      //print(dotSize);
                                    } else {
                                      if (scaler >= 22) {
                                        scaler =
                                            scaler - scaleDetails.scale / 2;
                                      } else {
                                        scaler = 22;
                                      }
                                      finalWidthOFSHEET =
                                          sheetWidthFactor * scaler;
                                      finalHightOFSHEET =
                                          (sheetHightFactor * scaler) + scaler;
                                      dotSize = (scaler) * 0.8;
                                      //print(dotSize);
                                    }
                                  });

                                  setState(() {
                                    afterCenterX =
                                        sheetWidthFactor * scaler / 2;
                                    afterCenterY =
                                        sheetHightFactor * scaler / 2;
                                  });

                                  if (scaleDetails.scale != 1) {
                                    scrollControllerX.jumpTo((sheetWidthFactor *
                                            scaler /
                                            2) -
                                        ((MediaQuery.of(context).size.width -
                                                10) /
                                            2) -
                                        (afterCenterX *
                                            (beforeCenterX - focusPosX) /
                                            beforeCenterX));

                                    scrollControllerY.jumpTo((sheetHightFactor *
                                            scaler /
                                            2) -
                                        ((MediaQuery.of(context).size.height -
                                                190) /
                                            2) -
                                        (afterCenterY *
                                            (beforeCenterY - focusPosY) /
                                            beforeCenterX));
                                  }
                                },
                                child: CustomPaint(
                                  size: Size(
                                      finalWidthOFSHEET, finalHightOFSHEET),
                                  willChange: false,
                                  painter: MyPainter(
                                    sheetWidthFactor,
                                    sheetHightFactor,
                                    dataSpreadXFactor,
                                    dataSpreadYFactor,
                                    scaler,
                                    dataSpreadFactorA,
                                    dataSpreadFactorB,
                                    graphInnerBoxXoffsetFactorLeft,
                                    graphInnerBoxXoffsetFactorRight,
                                    graphInnerBoxYoffsetFactorTop,
                                    graphInnerBoxYoffsetFactorBottom,
                                    finalWidthOFSHEET,
                                    finalHightOFSHEET,
                                    focusPosX,
                                    focusPosY,
                                    setArrowPositionOnTheFly,
                                    testPosX,
                                    testPosY,
                                    dotSize,
                                    scoreValues,
                                  ),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Stack(
                                      children: <Widget>[
                                        // Positioned(
                                        //   top: scaler *
                                        //           (graphInnerBoxYoffsetFactorTop +
                                        //               dataSpreadFactorA) -
                                        //       scaler * .5,
                                        //   child: Transform(
                                        //     child: Text(
                                        //       player1Name,
                                        //       style: TextStyle(
                                        //           fontSize: 25.0,
                                        //           fontWeight: FontWeight.w400),
                                        //     ),
                                        //     alignment: Alignment(-.98, -1),
                                        //     transform: new Matrix4.identity()
                                        //       ..rotateZ(-90 * 3.1415927 / 180),
                                        //   ),
                                        // ),
                                        // Positioned(
                                        //   top: scaler *
                                        //           (graphInnerBoxYoffsetFactorTop +
                                        //               dataSpreadFactorA) +
                                        //       100,
                                        //   child: Transform(
                                        //     child: SizedBox(
                                        //       height: 20,
                                        //       width: 100,
                                        //       child: Container(
                                        //         color: Colors.red,
                                        //         child: Text(
                                        //           player2Name,
                                        //           textAlign: TextAlign.right,
                                        //           style: TextStyle(
                                        //               fontSize: 25.0,
                                        //               fontWeight:
                                        //                   FontWeight.w400),
                                        //         ),
                                        //       ),
                                        //     ),
                                        //     alignment: Alignment(-.98, -1),
                                        //     transform: new Matrix4.identity()
                                        //       ..rotateZ(-90 * 3.1415927 / 180),
                                        //   ),
                                        // ),
                                        Positioned(
                                          left: upArrowPositionX,
                                          top: upArrowPositionY,
                                          child: Center(
                                            child: upArrowShow
                                                ? GestureDetector(
                                                    onTap: () {
                                                      changTheGraphDataOnTheFly(
                                                          'up');
                                                    },
                                                    child: Container(
                                                      // color: Colors.red,
                                                      height: arrowSize,
                                                      width: arrowSize,
                                                      child: upArrow,
                                                    ))
                                                : null,
                                          ),
                                        ),
                                        Positioned(
                                          left: settingPositionX,
                                          top: settingPositionY,
                                          child: Center(
                                            child: settingShow
                                                ? GestureDetector(
                                                    onTap: () {
                                                      changeSetttingsOnTheFly(
                                                          context);
                                                    },
                                                    child: Container(
                                                      // color: Colors.red,
                                                      height: settingSize,
                                                      width: settingSize,
                                                      child: settings,
                                                    ))
                                                : null,
                                          ),
                                        ),
                                        Positioned(
                                          left: downArrowPositionX,
                                          top: downArrowPositionY,
                                          child: Center(
                                            child: downArrowShow
                                                ? GestureDetector(
                                                    onTap: () {
                                                      changTheGraphDataOnTheFly(
                                                          'down');
                                                    },
                                                    child: Container(
                                                      // color: Colors.green,
                                                      height: arrowSize,
                                                      width: arrowSize,
                                                      child: downArrow,
                                                    ))
                                                : null,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: deviceWidth - 50,
                    height: 30,
                    //color: Colors.red,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          left: 1.0,
                          child: Container(
                            width: 50,
                            height: 30.0,
                            // color: Colors.teal,
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                  point[1].toString(),
                                style: TextStyle(
                                    fontSize: playerNameFontSize,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            player2Name,
                            style: TextStyle(
                                fontSize: playerNameFontSize,
                                fontWeight: FontWeight.w600,
                                color: Colors.black),
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    child: Column(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                //aces
                                Container(
                                  height: 30.0,
                                  width: 30.0,
                                  child: Center(
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40.0),
                                          color: Color(0xFFDB2828)),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  width: 120.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Aces",
                                      style: TextStyle(
                                          fontSize: chartkeyFontSize,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),

                                SizedBox(
                                  width: chartKeySizeBoxWidth,
                                ),

                                //firs serve
                                Container(
                                  height: 30.0,
                                  width: 30.0,
                                  child: Center(
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40.0),
                                          color: Color(0xFFA333C8)),
                                    ),
                                  ),
                                ),
                                Container(
                                  // height: 30.0,
                                  // width: 120.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "First Services",
                                      style: TextStyle(
                                          fontSize: chartkeyFontSize,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                //double fault
                                Container(
                                  height: 30.0,
                                  width: 30.0,
                                  child: Center(
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40.0),
                                          color: Color(0xFFFBBD08)),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  width: 120.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Double Fault",
                                      style: TextStyle(
                                          fontSize: chartkeyFontSize,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),

                                SizedBox(
                                  width: chartKeySizeBoxWidth,
                                ),

                                //second service
                                Container(
                                  height: 30.0,
                                  width: 30.0,
                                  child: Center(
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40.0),
                                          color: Color(0xFFF2711C)),
                                    ),
                                  ),
                                ),
                                Container(
                                  // height: 30.0,
                                  // width: 130.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Second Service",
                                      style: TextStyle(
                                          fontSize: chartkeyFontSize,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                //forced errors
                                Container(
                                  height: 30.0,
                                  width: 30.0,
                                  child: Center(
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40.0),
                                          color: Color(0xFF0931FC)),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  width: 120.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Forced Errors",
                                      style: TextStyle(
                                          fontSize: chartkeyFontSize,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),

                                SizedBox(
                                  width: chartKeySizeBoxWidth,
                                ),

                                //unforced errors
                                Container(
                                  height: 30.0,
                                  width: 30.0,
                                  child: Center(
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40.0),
                                          color: Color(0xFF000000)),
                                    ),
                                  ),
                                ),
                                Container(
                                  // height: 30.0,
                                  // width: 130.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Unforced Errors",
                                      style: TextStyle(
                                          fontSize: chartkeyFontSize,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                //Winners
                                Container(
                                  height: 30.0,
                                  width: 30.0,
                                  child: Center(
                                    child: Container(
                                      height: 20.0,
                                      width: 20.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(40.0),
                                          color: Color(0xFF00B5AD)),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 30.0,
                                  width: 120.0,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Winners",
                                      style: TextStyle(
                                          fontSize: chartkeyFontSize,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) async {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            switch (response['calledMethod']) {
              case 'getUserNames':
                List ddata = response['response_data'];
                String xplayer1Name = "";
                String xplayer2Name = "";
                for (var item in ddata) {
                  if (item['player_id'] == widget.arguments["player1"]) {
                    xplayer1Name = item['name'];
                  }
                  if (item['player_id'] == widget.arguments["player2"]) {
                    xplayer2Name = item['name'];
                  }
                }
                setState(() {
                  player1Name = xplayer1Name;
                  player2Name = xplayer2Name;
                });
                break;
              case 'getSocreData':
                List dataList = response['response_data'];
                // print(dataList);

                int positiveMaxVal = 0,
                    negativeMaxVal = 0,
                    positive = 0,
                    negative = 0,
                    chartLength = 0;

                List<int> pointArray = List(15);

                scoreValues = [];

                double x = deviceWidth / (dataList.length + 2);

                if (x < 22) {
                  setState(() {
                    if (deviceWidth >= 900) {
                      scaler = 50;
                    } else {
                      scaler = 22;
                    }
                    dotSize = scaler * 0.8;
                  });
                } else if (x >= 50 && deviceWidth < 900) {
                  setState(() {
                    scaler = 50;
                    dotSize = scaler * 0.8;
                  });
                } else if (x >= 80 && deviceWidth >= 900) {
                  setState(() {
                    scaler = 80;
                    dotSize = scaler * 0.8;
                  });
                } else {
                  setState(() {
                    scaler = x;
                    dotSize = scaler * 0.8;
                  });
                }

                int realChartLength = ((deviceWidth / scaler).round()) - 2;

                if (dataList.length != 0) {
                  point[0]= dataList[dataList.length -1]['player1Sets'];
                  point[1]=dataList[dataList.length -1]['player2Sets'];
                } else {                  
                  point[0]= 0;
                  point[1]= 0;
                }


                for (int i = 0; i < dataList.length; i++) {
                  chartLength++;
                  var item = dataList[i];
                  //table peek calculation
                  if (item['won'] == widget.arguments["player1"] &&
                      item['type'] != ScoreValues.fault) {
                    negative--;
                    positive++;

                    if (positiveMaxVal < positive) {
                      positiveMaxVal = positive;
                    }
                  } else if (item['won'] == widget.arguments["player2"] &&
                      item['type'] != ScoreValues.fault) {
                    positive--;
                    negative++;

                    if (negativeMaxVal < negative) {
                      negativeMaxVal = negative;
                    }
                  } else {
                    continue;
                  }
                  //0
                  pointArray[0] = 0;
                  //1
                  pointArray[1] = 0;
                  //2
                  pointArray[2] = item['rally'];

                  //set point Winner
                  if (item['won'] == widget.arguments["player1"]) {
                    //3
                    pointArray[3] = 1;
                    pointArray[9] = 1;
                  } else if (item['won'] == widget.arguments["player2"]) {
                    //3
                    pointArray[3] = -1;
                    pointArray[9] = -1;
                  }

                  //4
                  if (item['type'] == ScoreValues.ace) {
                    pointArray[4] = 1;
                  } else if (item['type'] == ScoreValues.dflt) {
                    pointArray[4] = 2;
                  } else if (item['type'] == ScoreValues.winner) {
                    pointArray[4] = 3;
                  } else if (item['type'] == ScoreValues.backHand) {
                    pointArray[4] = 4;
                  } else if (item['type'] == ScoreValues.backHandVolley) {
                    pointArray[4] = 5;
                  } else if (item['type'] == ScoreValues.foreHand) {
                    pointArray[4] = 6;
                  } else if (item['type'] == ScoreValues.forHandVolley) {
                    pointArray[4] = 7;
                  } else if (item['type'] == ScoreValues.smash) {
                    pointArray[4] = 8;
                  } else if (item['type'] == ScoreValues.uErr) {
                    pointArray[4] = 9;
                  } else if (item['type'] == ScoreValues.uErrBackHand) {
                    pointArray[4] = 10;
                  } else if (item['type'] == ScoreValues.uErrBackHandVolley) {
                    pointArray[4] = 11;
                  } else if (item['type'] == ScoreValues.uErrForeHand) {
                    pointArray[4] = 12;
                  } else if (item['type'] == ScoreValues.uErrForHandVolley) {
                    pointArray[4] = 13;
                  } else if (item['type'] == ScoreValues.uErrSmash) {
                    pointArray[4] = 14;
                  } else if (item['type'] == ScoreValues.fErr) {
                    pointArray[4] = 15;
                  } else if (item['type'] == ScoreValues.fErrBackHand) {
                    pointArray[4] = 16;
                  } else if (item['type'] == ScoreValues.fErrBackHandVolley) {
                    pointArray[4] = 17;
                  } else if (item['type'] == ScoreValues.fErrForeHand) {
                    pointArray[4] = 18;
                  } else if (item['type'] == ScoreValues.fErrForHandVolley) {
                    pointArray[4] = 19;
                  } else if (item['type'] == ScoreValues.fErrSmash) {
                    pointArray[4] = 20;
                  }

                  // Pointer(0, 0, 2, 1, 5, 0, 0, 0, 0, 0, 0, 0),
                  // Pointer(0, 0, 4, 1, 6,1, 1, 1, 0, 1, 1, 0),
                  if (item['gameFinished'] == 1) {
                    //5-> gameFinished
                    pointArray[5] = 1;

                    //6-> serve
                    if (item['serve'] == 1) {
                      pointArray[6] = 1;
                    } else {
                      pointArray[6] = -1;
                    }

                    //7-> player1Games
                    pointArray[7] = item['player1Games'];

                    //8-> player2Games
                    pointArray[8] = item['player2Games'];

                    //9-> gameWinner
                    //pointArray[9]=1;

                    //10-> setCount
                    // print(item['setFinished']);
                    if (item['setFinished'] == 1) {
                      pointArray[10] = item['totalSets'];
                    } else {
                      pointArray[10] = item['totalSets'] + 1;
                    }

                    //11-> setFinished
                    pointArray[11] = item['setFinished'];
                  } else {
                    pointArray[5] = 0;
                    pointArray[6] = 0;
                    pointArray[7] = 0;
                    pointArray[8] = 0;
                    pointArray[9] = 0;
                    pointArray[10] = 0;
                    pointArray[11] = 0;
                  }

                  // if (i != 0) {
                  //   if (dataList[i - 1]['type'] == ScoreValues.fault) {
                  //     //second seve
                  //     pointArray[12] = 2;
                  //   } else {
                  //     pointArray[12] = 1;

                  //     //first seerve
                  //   }
                  // } else {
                  //   if (item['type'] != ScoreValues.fault) {
                  //     pointArray[12] = 1;
                  //     //first serve
                  //   }
                  // }

                  if (item['serveChance'] == 1) {
                    pointArray[12] = 1;
                  } else {
                    pointArray[12] = 2;
                  }

                  scoreValues.add(Pointer(
                    0,
                    0,
                    pointArray[0].toDouble(),
                    pointArray[1].toDouble(),
                    pointArray[2],
                    pointArray[3],
                    pointArray[4],
                    pointArray[5],
                    pointArray[6],
                    pointArray[7],
                    pointArray[8],
                    pointArray[9],
                    pointArray[10],
                    pointArray[11],
                    pointArray[12],
                    item['id'],
                  ));

                  // 0-> xVal , 1->yVal , 2-> rallyCount , 3-> pointWinner , 4-> type , 5-> gameFinished , 6-> serve , 7-> player1Games , 8-> player2Games , 9-> gameWinner , 10-> setCount , 11-> setFinished , 12->serviceChance

                }

                if (chartLength < realChartLength) {
                  chartLength = realChartLength;
                }

                if (positiveMaxVal < 3) {
                  positiveMaxVal = 3;
                }

                if (negativeMaxVal < 3) {
                  negativeMaxVal = 3;
                }

                if (dataList.length == 0) {
                  if (deviceWidth < 900) {
                    scaler = 50;
                  } else {
                    scaler = 80;
                  }
                }

                setState(() {
                  dataSpreadFactorA = positiveMaxVal.toDouble();
                  dataSpreadFactorB = negativeMaxVal.toDouble();
                  noOfElement = chartLength.toDouble();

                  dataSpreadXFactor = noOfElement;
                  dataSpreadYFactor = dataSpreadFactorA + dataSpreadFactorB;
                  sheetWidthFactor = dataSpreadXFactor +
                      graphInnerBoxXoffsetFactorLeft +
                      graphInnerBoxXoffsetFactorRight;
                  sheetHightFactor = dataSpreadYFactor +
                      graphInnerBoxYoffsetFactorTop +
                      graphInnerBoxYoffsetFactorBottom;
                  finalWidthOFSHEET = sheetWidthFactor * scaler;
                  finalHightOFSHEET = sheetHightFactor * scaler;
                });

                getUserNames(
                    widget.arguments["player1"], widget.arguments["player2"]);
                break;
              // case 'updateGraphData':
              //   print("WorkDone");
              //   break;
              // case 'playerUPDOWNChange':
              //   print('dfdfld');
              //   break;
              default:
            }
          }
        }
    }
  }
}

class TypeClass {
  final String name;
  final Color color;
  final int type;
  TypeClass(this.name, this.color, this.type);
}
