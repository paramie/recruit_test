import 'package:flutter/cupertino.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:tenizo/custom/text_field.dart';
import 'package:tenizo/custom/drop_down.dart';
import 'package:tenizo/validation/textBoxValidation.dart';
import 'package:uuid/uuid.dart';

class GameSettingUpdate extends StatefulWidget {
  final scoreArguments;
  final Function onSelected;
  final Function gameDataUpdate;
  final Function toggleStartState;
  final gameNotxStarted;
  final String userName;

  @override
  _GameSettingUpdate createState() => _GameSettingUpdate();

  GameSettingUpdate(
      {Key key,
      this.gameNotxStarted,
      this.onSelected,
      this.gameDataUpdate,
      this.scoreArguments,
      this.toggleStartState,
      this.userName})
      : super(key: key);
}

class _GameSettingUpdate extends State<GameSettingUpdate>
    with BaseControllerListner {
  BaseController controller;
  FocusNode _focusPlayerA = new FocusNode();
  FocusNode _focusPlayerB = new FocusNode();
  FocusNode _focusMatchName = new FocusNode();
  FocusNode _focusNumberOfGames = new FocusNode();
  FocusNode _focusStadium = new FocusNode();
  FocusNode _focusCourtName = new FocusNode();
  int playerNo = 0;

  @override
  void initState() {
    super.initState();
    controller = new BaseController(this);

    _focusPlayerA.addListener(_onFocusChangePlayerA);
    _focusPlayerB.addListener(_onFocusChangePlayerB);
    _focusMatchName.addListener(_onFocusChangeMatchName);
    _focusNumberOfGames.addListener(_onFocusChangeNumberOfGames);
    _focusStadium.addListener(_onFocusChangeStadium);
    _focusCourtName.addListener(_onFocusChangeCourtName);
    focusValue = false;

    player1Id = widget.scoreArguments['player1Id'].toString();
    player2Id = widget.scoreArguments['player2Id'].toString();
    matchId = widget.scoreArguments['match_id'].toString();
    weather = widget.scoreArguments['weather'].toString();
    temp = widget.scoreArguments['temperature'].toString();
    stadium = widget.scoreArguments['stadium'].toString();
    tempCourt = widget.scoreArguments['court'].toString();
    tieBreak = widget.scoreArguments['tieBreak'].toString();
    advantage = widget.scoreArguments['advantage'].toString();
    noOfSets = widget.scoreArguments['noOfSets'].toString();
    dateObject = widget.scoreArguments['date'];
    timeObject = widget.scoreArguments['time'];
    displayedTime = timeFormatter(timeObject);
    _noOfGamesController.text = widget.scoreArguments['noOfGames'].toString();
    _noOfSetsController.text = widget.scoreArguments['noOfSets'].toString();
    _matchNameController.text = widget.scoreArguments['match_name'].toString();
    _tieBreakController.text = widget.scoreArguments['tieBreak'].toString();
    _advantageController.text = widget.scoreArguments['advantage'].toString();
    //print(widget.scoreArguments);
    _loadCourtData();
    _loadPlayerData();
    _loadStatdiumData();
    _temperature();
    _loadPlayerNumber();

    //print(widget.scoreArguments.toString());
    _validateMatchName = true;
    _validatePlayerAName = true;
    _validatePlayerBName = true;

    textBoderColor = AppColors.form_border;
    errorText = null;
    textBoderColorPlayerA = AppColors.form_border;
    errorTextPlayerA = null;
    textBoderColorPlayerB = AppColors.form_border;
    errorTextPlayerB = null;
    textBoderColorGameNum = AppColors.form_border;
    errorTextGameNum = null;
    textBoderColorNoOfSets = AppColors.form_border;
    errorTextNoOfSets = null;

    enableButton = false;
  }

  String matchId = '';
  String player1Id = '';
  String player2Id = '';
  String tempCourt = '';
  bool updatableValues = false;
  final _playerAController = new TextEditingController();
  final _playerBController = new TextEditingController();
  final _stadiumController = new TextEditingController();
  final _courtController = new TextEditingController();
  var playerAControllerValue = '';
  var playerBControllerValue = '';
  var stadiumValue = '';
  var courtValue = '';

  void _onFocusChangeCourtName() {
    if (_focusCourtName.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangeStadium() {
    if (_focusStadium.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangePlayerA() {
    if (_focusPlayerA.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangePlayerB() {
    if (_focusPlayerB.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangeMatchName() {
    if (_focusMatchName.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  void _onFocusChangeNumberOfGames() {
    if (_focusNumberOfGames.hasFocus) {
      setState(() {
        focusValue = true;
      });
    }
  }

  // LOAD PLAYER DATA --------------------------------------------
  _loadPlayerData() {
    var param1 = [
      "SELECT player_id,Player_no,name FROM player where user_name = ? and status = ?",
      [widget.userName, 0],
      {"calledMethod": "_loadPlayerData"}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param1);
  }

  //load player number
  _loadPlayerNumber() {
    var selectQuery = [
      "SELECT Player_no from player where user_name = ? ORDER BY Player_no DESC",
      [widget.userName],
      {"calledMethod": "selectPlayerNumber"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

  // LOAD STADIUM DATA --------------------------------------------
  _loadStatdiumData() {
    var param2 = [
      "SELECT stadium_id,stadium_name FROM stadium where user_name = ? and status = ?",
      [widget.userName, 0],
      {"calledMethod": "_loadStatdiumData"}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param2);
  }

  // LOAD COURT DATA --------------------------------------------
  _loadCourtData() {
    //print("load court data");
    //print(stadium);
    var param3 = [
      "SELECT court_id,court_name,stadium_id,stadium_name FROM court where stadium_id = ? and status = 0",
      [stadium],
      {"calledMethod": "_loadCourtDataDatabase"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param3);
  }

  //Dropdownvalue
  String drpDownA;
  String drpDownB;
  String drpDownAafterChange;
  String drpDownBafterChange;

  // //Display number
  String displayPlayerANo = "";
  String playerANoAfterChange = "";
  String displayPlayerBNo = "";
  String playerBNoAfterChange = "";

  // INITIALIZING STATE VALUES --------------------------------------------
  static var dateObject = DateTime.now();
  String displayedDate = dateFormatter(dateObject);
  static var timeObject = DateTime.now();
  String displayedTime = "00 : 00";
  String dbTime = "00:00";

  // ----------------------------------
  static const form_width = 280.0;
  static const row_height = 80.0;
  static const box_height = 48.0;
  static const default_font_size = AppStyles.normal_font;
  static const default_padding = 10.0;
  static const text_box_height = (box_height - default_font_size) / 2.0;

  static var disabledBtn = AppColors.ternary_color;
  static var disabledBtnFont = AppColors.black;
  bool btnEnbled = false;

  //Feild error colors
  static var textBoderColor = AppColors.form_border;
  static var errorText;
  static var textBoderColorPlayerA = AppColors.form_border;
  static var errorTextPlayerA;
  static var textBoderColorPlayerB = AppColors.form_border;
  static var errorTextPlayerB;
  static var textBoderColorGameNum = AppColors.form_border;
  static var errorTextGameNum;
  static var textBoderColorNoOfSets = AppColors.form_border;
  static var errorTextNoOfSets;
  static var textBoderColorAdvantage = AppColors.form_border;
  static var errorTextAdvantage;
  static var textBoderColorTieBreack = AppColors.form_border;
  static var errorTextTieBreack;

  //Validation variables
  bool _validateMatchName = true;
  bool _validatePlayerAName = true;
  bool _validatePlayerBName = true;

  static String messageMatchName = "Error in text";
  static String messagePlayerAName = "Error in text";
  static String messagePlayerBName = "Error in text";

  static bool enableButton = false;

  static int noOfCourt;
  static int noOfCourtRegisterd;

  static bool courtData = false;

  // DISABLED BTN--------------------------
  void checkEnabled() {
    if ((_matchNameController.text.toString() != '') &&
        (_noOfGamesController.text.toString() != '') &&
        (_playerBController.text.toString() != '') &&
        (_playerAController.text.toString() != '') &&
        (noOfSets != null) &&
        (advantage != null) &&
        (tieBreak != null) &&
        (noOfSets != '') &&
        (advantage != '') &&
        (tieBreak != '') &&
        _validateMatchName == true &&
        _validatePlayerAName == true &&
        _validatePlayerBName == true) {
      setState(() {
        enableButton = true;
        disabledBtn = AppColors.ternary_color;
        disabledBtnFont = AppColors.black;
        btnEnbled = true;
      });
    } else {
      setState(() {
        enableButton = false;
        disabledBtn = AppColors.gray;
        disabledBtnFont = AppColors.white;
        btnEnbled = false;
      });
    }
  }

  // SAVE DATA -------------------------------------------------------
  void updateData() {
    checkEnabled();
    //print(court);
    var updateQuery = [
      "UPDATE gameSettings SET match_name=? , player1=?, player2=?, no_of_sets=? , no_of_games=? ,wheather=? , advantage=? ,tie_break=? ,stadium=?,court_number=? ,game_date=? ,game_time=? WHERE match_id=?",
      [
        _matchNameController.text.toString(),
        player1Id,
        player2Id,
        noOfSets,
        _noOfGamesController.text.toString(),
        weather,
        advantage,
        tieBreak,
        stadium,
        court,
        dateObject.toString(),
        dbTime,
        matchId
      ],
      {"calledMethod": "updateData"}
    ];
    if (btnEnbled) {
      controller.execFunction(
          ControllerFunc.db_sqlite, ControllerSubFunc.db_update, updateQuery);
    }
  }

  void newDataInject() {
    int oneValueAtLeastActive = 0;
    List queries = [];
    List params = [];

    if (_playerAController.text != playerAControllerValue) {
      // Player A Update ------------------------------------
      player1Id = Uuid().v1();
      queries.add(
          "INSERT INTO player (user_name,player_id,Player_no,name, gender, date_of_birth, handedness, playstyle, team, role, status) VALUES (? , ? ,? , ? , ? , ? , ? , ?, ? , ? , ? )");
      params.add([
        widget.userName,
        player1Id,
        playerNo,
        _playerAController.text,
        CommonValues.notSet,
        '',
        CommonValues.notSet,
        CommonValues.notSet,
        '',
        CommonValues.notSet,
        '0'
      ]);
      oneValueAtLeastActive = 1;
      playerNo++;
    }
    if (_playerBController.text != playerBControllerValue) {
      player2Id = Uuid().v1();
      queries.add(
          "INSERT INTO player (user_name,player_id,Player_no,name, gender, date_of_birth, handedness, playstyle, team, role, status) VALUES ( ? , ? , ? , ? , ? , ? , ? , ?, ? , ? , ? )");
      params.add([
        widget.userName,
        player2Id,
        playerNo,
        _playerBController.text,
        CommonValues.notSet,
        '',
        CommonValues.notSet,
        CommonValues.notSet,
        '',
        CommonValues.notSet,
        '0'
      ]);
      oneValueAtLeastActive = 1;
    }

    if (_stadiumController.text != stadiumValue &&
        _stadiumController.text.length != 0) {
      stadium = Uuid().v1();
      queries.add(
          "INSERT INTO stadium (user_name,stadium_id, stadium_name, stadium_address, contact_number, no_of_courts, reservation_url, Stadium_Lat, Stadium_Lon, status) VALUES (? , ? , ? , ? , ? , ?, ? , ?,  ?, ? )");
      params.add([
        widget.userName,
        stadium,
        _stadiumController.text,
        '',
        '-',
        0,
        '',
        0,
        0,
        0
      ]);
      oneValueAtLeastActive = 1;
    }

    if (stadium != null &&
        _courtController.text != courtValue &&
        _courtController.text != '' &&
        _courtController.text != null) {
      court = Uuid().v1();
      queries.add(
          "INSERT INTO court (court_id,stadium_id,court_name, serface, indoor, available_time, stadium_name,status) VALUES ( ? , ? , ? , ? , ?, ? , ? , ? )");
      params.add([
        court,
        stadium,
        _courtController.text,
        CommonValues.notSet,
        CommonValues.notSet,
        '0-0',
        _stadiumController.text,
        '0'
      ]);
      oneValueAtLeastActive = 1;
      courtData = true;
    }

    if (oneValueAtLeastActive == 1) {
      var querywithParams = [
        queries,
        params,
        {"calledMethod": 'newDataInject'}
      ];

      controller.execFunction(ControllerFunc.db_sqlite,
          ControllerSubFunc.db_select_batch, querywithParams);
    } else {
      updateData();
    }
  }

  void upDateResult() {
    var updateQuery = [
      "UPDATE result SET winner=? , looser=? WHERE match_id=?",
      [player1Id, player2Id, matchId],
      {"calledMethod": "upDateResult"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_update, updateQuery);
  }

  // GET DATA -------------------------------------------------------
  void getData(x) {
    var selectQuery = [
      "SELECT * FROM gameSettings WHERE match_id=?",
      [x],
      {"calledMethod": "getData"}
    ];
    controller.execFunction(
      ControllerFunc.db_sqlite,
      ControllerSubFunc.db_select,
      selectQuery,
    );
  }

  // VERTICAL ITEMPADDING --------------------------------------------
  commonPadding() {
    return EdgeInsets.fromLTRB(0.0, default_padding, 0.0, 0.0);
  }

  // INPUT BOX UPPER TEXT --------------------------------------------
  customFont(text) {
    return Flexible(
      child: Text(
        text,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontSize: default_font_size,
            color: AppColors.black,
            fontWeight: FontWeight.w600),
      ),
    );
  }

  static dateFormatter(date) {
    var selectedyear = date.year;
    var selectedmonth = 'Jan';
    var selectedDate = date.day;
    switch (date.month) {
      case 1:
        selectedmonth = 'Jan';
        break;
      case 2:
        selectedmonth = 'Feb';
        break;
      case 3:
        selectedmonth = 'Mar';
        break;
      case 4:
        selectedmonth = 'Apr';
        break;
      case 5:
        selectedmonth = 'May';
        break;
      case 6:
        selectedmonth = 'Jun';
        break;
      case 7:
        selectedmonth = 'Jul';
        break;
      case 8:
        selectedmonth = 'Aug';
        break;
      case 9:
        selectedmonth = 'Sept';
        break;
      case 10:
        selectedmonth = 'Oct';
        break;
      case 11:
        selectedmonth = 'Nov';
        break;
      case 12:
        selectedmonth = 'Dec';
        break;
      default:
    }
    return '$selectedmonth $selectedDate , $selectedyear';
  }

  // Time FORMAT (eg: 00:00:00) --------------------------------------------

  static timeFormatter(date) {
    var hr = date.hour;
    if (hr < 10) {
      hr = '0$hr';
    }
    var min = date.minute;
    if (min < 10) {
      min = '0$min';
    }
    return '$hr : $min';
  }

  static dbtimeFormatter(date) {
    var hr = date.hour;
    if (hr < 10) {
      hr = '0$hr';
    }
    var min = date.minute;
    if (min < 10) {
      min = '0$min';
    }
    return '$hr:$min';
  }

  // VERTICAL ITEM - DATE_ADDER --------------------------------------------
  customDate(text, saveIndex) {
    return Align(
      child: Container(
        width: form_width,
        height: row_height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(child: customFont(text)),
            Container(
              width: double.infinity,
              height: 48,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all(
                  color: AppColors.form_border,
                  width: 1.2,
                  style: BorderStyle.solid,
                ),
              ),
              child: FlatButton(
                onPressed: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2016, 1, 1),
                      maxTime: DateTime(2025, 12, 31),
                      onChanged: (date) {}, onConfirm: (date) {
                    dateFormatter(date);
                    setState(() {
                      dateObject = date;
                      displayedDate = dateFormatter(date);
                    });
                  }, currentTime: dateObject, locale: LocaleType.en);
                },
                child: Center(
                  child: Text(
                    displayedDate,
                    style: TextStyle(
                        fontSize: default_font_size,
                        color: AppColors.black,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  DateTime time = DateTime.now();

  // VERTICAL ITEM - TIME_ADDER --------------------------------------------
  customTime(text, saveIndex) {
    if (displayedTime == "00 : 00") {
      time = DateTime.now();
    } else {
      time = DateTime.parse(timeObject.toString());
    }

    return Align(
      child: Container(
        width: form_width,
        height: row_height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(child: customFont(text)),
            Container(
                width: double.infinity,
                height: 48,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                    color: AppColors.form_border,
                    width: 1.2,
                    style: BorderStyle.solid,
                  ),
                ),
                child: FlatButton(
                  onPressed: () {
                    showCupertinoModalPopup<void>(
                      context: context,
                      builder: (BuildContext context) {
                        return _buildBottomPicker(
                          CupertinoDatePicker(
                            use24hFormat: true,
                            mode: CupertinoDatePickerMode.time,
                            initialDateTime: time,
                            onDateTimeChanged: (date) {
                              setState(() {
                                timeObject = date;
                                dbTime = dbtimeFormatter(date);
                                displayedTime = timeFormatter(date);
                              });
                            },
                          ),
                        );
                      },
                    );
                  },
                  child: Center(
                    child: Text(
                      displayedTime,
                      style: TextStyle(
                          fontSize: default_font_size,
                          color: AppColors.black,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomPicker(Widget picker) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                height: 44,
                child: CupertinoButton(
                  pressedOpacity: 0.3,
                  padding: EdgeInsets.only(right: 16, top: 0),
                  child: Text(
                    'Done',
                    style: TextStyle(color: Colors.transparent, fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 200,
          padding: const EdgeInsets.only(top: 0.0),
          color: CupertinoColors.white,
          child: DefaultTextStyle(
            style: const TextStyle(
              color: CupertinoColors.black,
              fontSize: 22.0,
            ),
            child: SafeArea(
              top: true,
              child: picker,
            ),
          ),
        ),
      ],
    );
  }

  commonTxtStyle() {
    return TextStyle(
      fontSize: default_font_size,
      fontFamily: 'Rajdhani',
      color: AppColors.black,
      fontWeight: FontWeight.w500,
    );
  }

  String noOfSets;
  String weather;
  String temp;
  String advantage;
  String tieBreak;
  String stadium;
  String court;
  final _matchNameController = new TextEditingController();
  final _noOfGamesController = new TextEditingController();
  final _noOfSetsController = new TextEditingController();
  final _tieBreakController = new TextEditingController();
  final _advantageController = new TextEditingController();

  List<KeyValueModel> playerNameList = [];
  List<KeyValueModel> stadiumNameList = [];
  List<KeyValueModel> courtNameList = [];
  bool focusValue = true;

  static var arrNoOFSets = <String>['1', '3', '5'];
  static var arrWeather = <String>[
    CommonValues.notSet,
    CommonValues.clear,
    CommonValues.cloudy,
    CommonValues.rainy,
    CommonValues.snow,
  ];

  static var tempr = <String>[];

  _temperature() {
    List<String> list = new List<String>();
    list.add("Not set");
    for (var i = 7; i < 45; i++) {
      String count = (i + 1).toString();
      list.add(count + " C");
    }
    setState(() {
      tempr = list;
    });
  }

  static var arrAdvantage = <String>[
    ScoreValues.noAdvantage,
    ScoreValues.advantage,
    ScoreValues.semiAdvantage,
  ];
  static var arrTieBreak = <String>[
    ScoreValues.noTieBreak,
    ScoreValues.tieBreak,
    ScoreValues.superTieBreak
  ];

  List<DropdownMenuItem<KeyValueModel>> loadPlayerList(player) {
    List<DropdownMenuItem<KeyValueModel>> dynamicList = [];

    switch (player) {
      case "A":
        for (var i = 0; i < playerNameList.length; i++) {
          if (playerNameList[i].key == drpDownB) {
            continue;
          }

          dynamicList.add(DropdownMenuItem<KeyValueModel>(
            child: Text(
              playerNameList[i].key,
              overflow: TextOverflow.fade,
              maxLines: 1,
              softWrap: false,
            ),
            value: playerNameList[i],
          ));
        }
        break;

      case "B":
        for (var i = 0; i < playerNameList.length; i++) {
          if (playerNameList[i].key == drpDownA) {
            continue;
          }

          dynamicList.add(DropdownMenuItem<KeyValueModel>(
            child: Text(
              playerNameList[i].key,
              overflow: TextOverflow.fade,
              maxLines: 1,
              softWrap: false,
            ),
            value: playerNameList[i],
          ));
        }
        break;
      default:
        break;
    }

    return dynamicList;
  }

  // MAIN PROGRAMME --------------------------------------------
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: Container(
            color: AppColors.white,
            child: ListView(
              children: <Widget>[
                // MATCH NAME AND ROUND ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: Container(
                    child: TxtBox(
                      topic: 'Match name and round',
                      borderColor: textBoderColor,
                      errorText: errorText,
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            right: 0,
                            top: 0,
                            child: Container(
                              width: box_height,
                              height: box_height,
                              child: Center(
                                child: FlatButton(
                                  onPressed: () {
                                    _matchNameController.clear();

                                    var errorStatus = TextBoxValidation.isEmpty(
                                        _matchNameController.text);

                                    setState(() {
                                      _validateMatchName = errorStatus['state'];
                                      messageMatchName =
                                          errorStatus['errorMessage'];
                                    });

                                    if (errorStatus['state'] == false) {
                                      setState(() {
                                        textBoderColor = Colors.red;
                                        errorText = errorStatus['errorMessage'];
                                      });
                                    } else if (errorStatus['state'] == true) {
                                      setState(() {
                                        textBoderColor = AppColors.form_border;
                                        errorText = null;
                                      });
                                    }

                                    checkEnabled();
                                  },
                                  child: Icon(Icons.close,
                                      size: 20, color: AppColors.black),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            right: 0,
                            child: Center(
                              child: focusValue
                                  ? Container(
                                      width: box_height,
                                      height: box_height,
                                      child: GestureDetector(
                                        onTap: () {
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                          setState(() {
                                            focusValue = false;
                                          });
                                        },
                                        child: Container(
                                            color: Colors.transparent),
                                      ),
                                    )
                                  : null,
                            ),
                          ),
                          Theme(
                            data: Theme.of(context).copyWith(),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(
                                  10.0, 25.0, (box_height - 5.0), 0.0),
                              child: TextField(
                                maxLength: 25,
                                focusNode: _focusMatchName,
                                controller: _matchNameController,
                                autofocus: false,
                                onChanged: (String newValue) {
                                  var errorStatus = TextBoxValidation.isEmpty(
                                      _matchNameController.text);

                                  setState(() {
                                    _validateMatchName = errorStatus['state'];
                                    messageMatchName =
                                        errorStatus['errorMessage'];
                                  });

                                  if (errorStatus['state'] == false) {
                                    setState(() {
                                      textBoderColor = Colors.red;
                                      errorText = errorStatus['errorMessage'];
                                    });
                                  } else if (errorStatus['state'] == true) {
                                    setState(() {
                                      textBoderColor = AppColors.form_border;
                                      errorText = null;
                                    });
                                  }

                                  checkEnabled();
                                },
                                style: TextStyle(
                                    fontSize: default_font_size,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w500),
                                decoration: InputDecoration(
                                  counterText: "",
                                  hintText: 'Enter match name...',
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: text_box_height),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                // PLAYER A NEW------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: Container(
                    child: TxtBox(
                      topic: 'Player A',
                      borderColor: textBoderColorPlayerA,
                      errorText: errorTextPlayerA,
                      child: Container(
                        decoration: updatableValues
                            ? null
                            : BoxDecoration(
                                color: AppColors.lightGray,
                                borderRadius: BorderRadius.circular(4.0)),
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0, top: 0),
                              child: Text(
                                displayPlayerANo,
                                style: commonTxtStyle(),
                              ),
                            ),
                            Flexible(
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                    right: 0,
                                    top: 0,
                                    child: Container(
                                      width: box_height,
                                      height: box_height,
                                      child: Center(
                                        child: Icon(Icons.expand_more,
                                            size: 24, color: Colors.black),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    left: 0,
                                    top: 0,
                                    width: form_width,
                                    height: box_height,
                                    child: Theme(
                                      data: Theme.of(context).copyWith(),
                                      child: DropdownButtonHideUnderline(
                                        child: Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              default_padding,
                                              0.0,
                                              default_padding,
                                              0.0),
                                          child: DropdownButton<KeyValueModel>(
                                            isExpanded: true,
                                            iconSize: 0,
                                            style: commonTxtStyle(),
                                            onChanged:
                                                (KeyValueModel newValue) {
                                              setNameandNo(newValue, "A");

                                              var errorStatus =
                                                  TextBoxValidation.isEmpty(
                                                      _playerAController.text);

                                              setState(() {
                                                _validatePlayerAName =
                                                    errorStatus['state'];
                                                messagePlayerAName =
                                                    errorStatus['errorMessage'];
                                              });

                                              if (errorStatus['state'] ==
                                                  false) {
                                                setState(() {
                                                  textBoderColorPlayerA =
                                                      Colors.red;
                                                  errorTextPlayerA =
                                                      errorStatus[
                                                          'errorMessage'];
                                                });
                                              } else if (errorStatus['state'] ==
                                                  true) {
                                                setState(() {
                                                  textBoderColorPlayerA =
                                                      AppColors.form_border;
                                                  errorTextPlayerA = null;
                                                });
                                              }

                                              checkEnabled();
                                            },
                                            items: loadPlayerList("A"),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    child: Center(
                                      child: focusValue
                                          ? Container(
                                              width: box_height,
                                              height: box_height,
                                              child: GestureDetector(
                                                onTap: () {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                  setState(() {
                                                    focusValue = false;
                                                  });
                                                },
                                                child: Container(
                                                    color: Colors.transparent),
                                              ),
                                            )
                                          : null,
                                    ),
                                  ),
                                  Theme(
                                    data: Theme.of(context).copyWith(),
                                    child: Container(
                                      decoration: updatableValues
                                          ? null
                                          : BoxDecoration(
                                              color: AppColors.lightGray,
                                              borderRadius:
                                                  BorderRadius.circular(4.0)),
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(
                                            2.0, 0.0, (box_height - 5.0), 0.0),
                                        child: TextField(
                                          maxLength: 25,
                                          autofocus: false,
                                          focusNode: _focusPlayerA,
                                          onChanged: (String newValue) {
                                            nameCleared(newValue, "A");

                                            var errorStatus =
                                                TextBoxValidation.isEmpty(
                                                    _playerAController.text);

                                            if (errorStatus['state'] == false) {
                                              setState(() {
                                                _validatePlayerAName =
                                                    errorStatus['state'];
                                                messagePlayerAName =
                                                    errorStatus['errorMessage'];

                                                textBoderColorPlayerA =
                                                    Colors.red;
                                                errorTextPlayerA =
                                                    errorStatus['errorMessage'];
                                              });
                                            } else if (errorStatus['state'] ==
                                                true) {
                                              _validatePlayerAName =
                                                  errorStatus['state'];
                                              messagePlayerAName =
                                                  errorStatus['errorMessage'];

                                              textBoderColorPlayerA =
                                                  AppColors.form_border;
                                              errorTextPlayerA = null;
                                            }

                                            checkEnabled();
                                          },
                                          controller: _playerAController,
                                          enabled: updatableValues,
                                          style: TextStyle(
                                              fontSize: default_font_size,
                                              color: AppColors.black,
                                              fontWeight: FontWeight.w500),
                                          decoration: InputDecoration(
                                            counterText: "",
                                            // hintText: 'Enter player A...',
                                            contentPadding:
                                                const EdgeInsets.symmetric(
                                                    vertical:
                                                        ((text_box_height /
                                                            2))),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),

                // PLAYER B NEW------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: Container(
                    child: TxtBox(
                      topic: 'Player B',
                      borderColor: textBoderColorPlayerB,
                      errorText: errorTextPlayerB,
                      child: Container(
                        decoration: updatableValues
                            ? null
                            : BoxDecoration(
                                color: AppColors.lightGray,
                                borderRadius: BorderRadius.circular(4.0)),
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0, top: 0),
                              child: Text(
                                displayPlayerBNo,
                                style: commonTxtStyle(),
                              ),
                            ),
                            Flexible(
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                    right: 0,
                                    top: 0,
                                    child: Container(
                                      width: box_height,
                                      height: box_height,
                                      child: Center(
                                        child: Icon(Icons.expand_more,
                                            size: 24, color: Colors.black),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    left: 0,
                                    top: 0,
                                    width: form_width,
                                    height: box_height,
                                    child: Theme(
                                      data: Theme.of(context).copyWith(),
                                      child: DropdownButtonHideUnderline(
                                        child: Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              default_padding,
                                              0.0,
                                              default_padding,
                                              0.0),
                                          child: DropdownButton<KeyValueModel>(
                                            isExpanded: true,
                                            iconSize: 0,
                                            style: commonTxtStyle(),
                                            onChanged:
                                                (KeyValueModel newValue) {
                                              setNameandNo(newValue, "B");

                                              var errorStatus =
                                                  TextBoxValidation.isEmpty(
                                                      _playerBController.text);

                                              setState(() {
                                                _validatePlayerBName =
                                                    errorStatus['state'];
                                                messagePlayerBName =
                                                    errorStatus['errorMessage'];
                                              });

                                              if (errorStatus['state'] ==
                                                  false) {
                                                setState(() {
                                                  textBoderColorPlayerB =
                                                      Colors.red;
                                                  errorTextPlayerB =
                                                      errorStatus[
                                                          'errorMessage'];
                                                });
                                              } else if (errorStatus['state'] ==
                                                  true) {
                                                setState(() {
                                                  textBoderColorPlayerB =
                                                      AppColors.form_border;
                                                  errorTextPlayerB = null;
                                                });
                                              }

                                              checkEnabled();
                                            },
                                            items: loadPlayerList("B"),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    child: Center(
                                      child: focusValue
                                          ? Container(
                                              width: box_height,
                                              height: box_height,
                                              child: GestureDetector(
                                                onTap: () {
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                  setState(() {
                                                    focusValue = false;
                                                  });
                                                },
                                                child: Container(
                                                    color: Colors.transparent),
                                              ),
                                            )
                                          : null,
                                    ),
                                  ),
                                  Theme(
                                    data: Theme.of(context).copyWith(),
                                    child: Container(
                                      decoration: updatableValues
                                          ? null
                                          : BoxDecoration(
                                              color: AppColors.lightGray,
                                              borderRadius:
                                                  BorderRadius.circular(4.0)),
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(
                                            2.0, 0.0, (box_height - 5.0), 0.0),
                                        child: TextField(
                                          maxLength: 25,
                                          focusNode: _focusPlayerB,
                                          autofocus: false,
                                          onChanged: (String newValue) {
                                            nameCleared(newValue, "B");

                                            var errorStatus =
                                                TextBoxValidation.isEmpty(
                                                    _playerBController.text);

                                            if (errorStatus['state'] == false) {
                                              setState(() {
                                                _validatePlayerBName =
                                                    errorStatus['state'];
                                                messagePlayerBName =
                                                    errorStatus['errorMessage'];

                                                textBoderColorPlayerB =
                                                    Colors.red;
                                                errorTextPlayerB =
                                                    errorStatus['errorMessage'];
                                              });
                                            }
                                            if (errorStatus['state'] == true) {
                                              _validatePlayerBName =
                                                  errorStatus['state'];
                                              messagePlayerBName =
                                                  errorStatus['errorMessage'];

                                              textBoderColorPlayerB =
                                                  AppColors.form_border;
                                              errorTextPlayerB = null;
                                            }

                                            checkEnabled();
                                          },
                                          controller: _playerBController,
                                          enabled: updatableValues,
                                          style: TextStyle(
                                              fontSize: default_font_size,
                                              color: AppColors.black,
                                              fontWeight: FontWeight.w500),
                                          decoration: InputDecoration(
                                            counterText: "",
                                            // hintText: 'Enter player B...',
                                            contentPadding:
                                                const EdgeInsets.symmetric(
                                                    vertical:
                                                        ((text_box_height /
                                                            2))),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),

                // NO OF GAMES ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: Container(
                    child: TxtBox(
                      topic: 'Number of games',
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            right: 0,
                            top: 0,
                            child: Container(
                              width: box_height,
                              height: box_height,
                              child: Center(
                                child: FlatButton(
                                  onPressed: () {
                                    _noOfGamesController.clear();
                                    checkEnabled();
                                  },
                                  child: Icon(Icons.close,
                                      size: 20, color: AppColors.black),
                                ),
                              ),
                            ),
                          ),
                          Theme(
                            data: Theme.of(context).copyWith(),
                            child: Container(
                              decoration: updatableValues
                                  ? null
                                  : BoxDecoration(
                                      color: AppColors.lightGray,
                                      borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(
                                    10.0, 25.0, (box_height - 5.0), 0.0),
                                child: TextField(
                                  focusNode: _focusNumberOfGames,
                                  keyboardType: TextInputType.number,
                                  autofocus: false,
                                  onChanged: (String newValue) {
                                    checkEnabled();
                                  },
                                  controller: _noOfGamesController,
                                  enabled: updatableValues,
                                  style: TextStyle(
                                      fontSize: default_font_size,
                                      color: AppColors.black,
                                      fontWeight: FontWeight.w500),
                                  decoration: InputDecoration(
                                    hintText: 'Enter the number of games...',
                                    contentPadding: const EdgeInsets.symmetric(
                                        vertical: text_box_height),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                // NO OF SETS ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: updatableValues
                      ? Container(
                          child: DrpDown(
                            topic: 'No of sets',
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: noOfSets,
                              iconSize: 0,
                              hint: Text('Set no of sets...'),
                              style: commonTxtStyle(),
                              onChanged: (String newValue) {
                                setState(() {
                                  noOfSets = newValue;
                                });
                                checkEnabled();
                              },
                              items: arrNoOFSets.map<DropdownMenuItem<String>>(
                                (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                        )
                      : Container(
                          child: TxtBox(
                            topic: 'No of sets',
                            child: Stack(
                              children: <Widget>[
                                Positioned(
                                  right: 0,
                                  top: 0,
                                  child: Container(
                                    width: box_height,
                                    height: box_height,
                                    child: Center(
                                      child: FlatButton(
                                        onPressed: () {
                                          _noOfSetsController.clear();
                                          checkEnabled();
                                        },
                                        child: Icon(Icons.close,
                                            size: 20, color: AppColors.black),
                                      ),
                                    ),
                                  ),
                                ),
                                Theme(
                                  data: Theme.of(context).copyWith(),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.lightGray,
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          10.0, 25.0, (box_height - 5.0), 0.0),
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        autofocus: false,
                                        onChanged: (String newValue) {
                                          checkEnabled();
                                        },
                                        controller: _noOfSetsController,
                                        enabled: false,
                                        style: TextStyle(
                                            fontSize: default_font_size,
                                            color: AppColors.black,
                                            fontWeight: FontWeight.w500),
                                        decoration: InputDecoration(
                                          hintText:
                                              'Enter the number of sets...',
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  vertical: text_box_height),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                ),

                // WHEATHER ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        child: DrpDown(
                          topic: 'Weather',
                          child: DropdownButton<String>(
                            isExpanded: true,
                            value: weather,
                            iconSize: 0,
                            hint: Text('Set the whather type...'),
                            style: commonTxtStyle(),
                            onChanged: (String newValue) {
                              setState(() {
                                weather = newValue;
                              });
                              checkEnabled();
                            },
                            items: arrWeather.map<DropdownMenuItem<String>>(
                              (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              },
                            ).toList(),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(65.0, 0.0, 65, 0.0),
                        child: focusValue
                            ? Container(
                                height: box_height + 30,
                                child: GestureDetector(
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    setState(() {
                                      focusValue = false;
                                    });
                                  },
                                  child: Container(color: Colors.transparent),
                                ),
                              )
                            : null,
                      ),
                    ],
                  ),
                ),

                // TEMPERATURE ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        child: DrpDown(
                          topic: 'Temperature in Celsius',
                          child: DropdownButton<String>(
                            isExpanded: true,
                            value: temp,
                            iconSize: 0,
                            hint: Text('Enter the temperature...'),
                            style: commonTxtStyle(),
                            onChanged: (String newValue) {
                              setState(() {
                                temp = newValue;
                              });
                              checkEnabled();
                            },
                            items: tempr.map<DropdownMenuItem<String>>(
                              (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              },
                            ).toList(),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(65.0, 0.0, 65, 0.0),
                        child: focusValue
                            ? Container(
                                height: box_height + 30,
                                child: GestureDetector(
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    setState(() {
                                      focusValue = false;
                                    });
                                  },
                                  child: Container(color: Colors.transparent),
                                ),
                              )
                            : null,
                      ),
                    ],
                  ),
                ),

                // ADVANTAGE ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: updatableValues
                      ? Container(
                          child: DrpDown(
                            topic: 'Advantage',
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: advantage,
                              iconSize: 0,
                              hint: Text('Set advantage...'),
                              style: commonTxtStyle(),
                              onChanged: (String newValue) {
                                setState(() {
                                  advantage = newValue;
                                });
                                checkEnabled();
                              },
                              items: arrAdvantage.map<DropdownMenuItem<String>>(
                                (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                        )
                      : Container(
                          child: TxtBox(
                            topic: 'Advantage',
                            child: Stack(
                              children: <Widget>[
                                Theme(
                                  data: Theme.of(context).copyWith(),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.lightGray,
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          10.0, 25.0, (box_height - 5.0), 0.0),
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        autofocus: false,
                                        onChanged: (String newValue) {
                                          checkEnabled();
                                        },
                                        controller: _advantageController,
                                        enabled: false,
                                        style: TextStyle(
                                            fontSize: default_font_size,
                                            color: AppColors.black,
                                            fontWeight: FontWeight.w500),
                                        decoration: InputDecoration(
                                          hintText: 'Enter advantage...',
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  vertical: text_box_height),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                ),
                // TIE BREAK ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: updatableValues
                      ? Container(
                          child: DrpDown(
                            topic: 'Tie break',
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: tieBreak,
                              iconSize: 0,
                              hint: Text('Set tie break...'),
                              style: commonTxtStyle(),
                              onChanged: (String newValue) {
                                setState(() {
                                  tieBreak = newValue;
                                });
                                checkEnabled();
                              },
                              items: arrTieBreak.map<DropdownMenuItem<String>>(
                                (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                        )
                      : Container(
                          child: TxtBox(
                            topic: 'Tie break',
                            child: Stack(
                              children: <Widget>[
                                Theme(
                                  data: Theme.of(context).copyWith(),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.lightGray,
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          10.0, 25.0, (box_height - 5.0), 0.0),
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        autofocus: false,
                                        onChanged: (String newValue) {
                                          checkEnabled();
                                        },
                                        controller: _tieBreakController,
                                        enabled: false,
                                        style: TextStyle(
                                            fontSize: default_font_size,
                                            color: AppColors.black,
                                            fontWeight: FontWeight.w500),
                                        decoration: InputDecoration(
                                          hintText: 'Enter tie break...',
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  vertical: text_box_height),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                ),

                // STADIUM ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: Container(
                    child: TxtBox(
                      topic: 'Stadium',
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            right: 0,
                            top: 0,
                            child: Container(
                              width: box_height,
                              height: box_height,
                              child: Center(
                                child: Icon(Icons.expand_more,
                                    size: 24, color: Colors.black),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0,
                            top: 0,
                            width: form_width,
                            height: box_height,
                            child: Theme(
                              data: Theme.of(context).copyWith(),
                              child: DropdownButtonHideUnderline(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(default_padding,
                                      0.0, default_padding, 0.0),
                                  child: DropdownButton<KeyValueModel>(
                                    isExpanded: true,
                                    iconSize: 0,
                                    style: commonTxtStyle(),
                                    onChanged: (KeyValueModel newValue) {
                                      setState(() {
                                        _stadiumController.text = newValue.key;
                                        stadiumValue = newValue.key;
                                        stadium = newValue.value;
                                      });

                                      _courtController.clear();
                                      _loadCourtData();
                                      checkEnabled();
                                    },
                                    items: stadiumNameList
                                        .map((KeyValueModel item) {
                                      return DropdownMenuItem<KeyValueModel>(
                                        child: Text(
                                          item.key,
                                          overflow: TextOverflow.fade,
                                          maxLines: 1,
                                          softWrap: false,
                                        ),
                                        value: item,
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            right: 0,
                            child: Center(
                              child: focusValue
                                  ? Container(
                                      width: box_height,
                                      height: box_height,
                                      child: GestureDetector(
                                        onTap: () {
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                          setState(() {
                                            focusValue = false;
                                          });
                                        },
                                        child: Container(
                                            color: Colors.transparent),
                                      ),
                                    )
                                  : null,
                            ),
                          ),
                          Theme(
                            data: Theme.of(context).copyWith(),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(
                                  10.0, 2.5, (box_height - 5.0), 0.0),
                              child: TextField(
                                maxLength: 25,
                                focusNode: _focusStadium,
                                autofocus: false,
                                onChanged: (String newValue) {
                                  var hasItem = stadiumNameList.firstWhere(
                                      (KeyValueModel item) =>
                                          item.key == newValue,
                                      orElse: () => null);

                                  if (hasItem != null) {
                                    setState(() {
                                      stadiumValue = hasItem.key;
                                      stadium = hasItem.value;
                                    });
                                    _courtController.clear();
                                    _loadCourtData();
                                  } else {
                                    _courtController.clear();
                                    clearCourtData();
                                    setState(() {
                                      stadium = newValue;
                                    });
                                  }
                                  checkEnabled();
                                },
                                controller: _stadiumController,
                                style: TextStyle(
                                    fontSize: default_font_size,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w500),
                                decoration: InputDecoration(
                                  counterText: "",
                                  hintText: 'Enter stadium...',
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: ((text_box_height / 2))),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                // COURT ------------------------------------------------------------
                Padding(
                  padding: commonPadding(),
                  child: Container(
                    child: TxtBox(
                      topic: 'Court Name / NO',
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            right: 0,
                            top: 0,
                            child: Container(
                              width: box_height,
                              height: box_height,
                              child: Center(
                                child: Icon(Icons.expand_more,
                                    size: 24, color: Colors.black),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0,
                            top: 0,
                            width: form_width,
                            height: box_height,
                            child: Theme(
                              data: Theme.of(context).copyWith(),
                              child: DropdownButtonHideUnderline(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(default_padding,
                                      0.0, default_padding, 0.0),
                                  child: DropdownButton<KeyValueModel>(
                                    isExpanded: true,
                                    iconSize: 0,
                                    style: commonTxtStyle(),
                                    onChanged: (KeyValueModel newValue) {
                                      setState(() {
                                        _courtController.text = newValue.key;
                                        courtValue = newValue.key;
                                        court = newValue.value;
                                      });
                                      _loadCourtData();
                                      checkEnabled();
                                    },
                                    items:
                                        courtNameList.map((KeyValueModel item) {
                                      return DropdownMenuItem<KeyValueModel>(
                                        child: Text(
                                          item.key,
                                          overflow: TextOverflow.fade,
                                          maxLines: 1,
                                          softWrap: false,
                                        ),
                                        value: item,
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            right: 0,
                            child: Center(
                              child: focusValue
                                  ? Container(
                                      width: box_height,
                                      height: box_height,
                                      child: GestureDetector(
                                        onTap: () {
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                          setState(() {
                                            focusValue = false;
                                          });
                                        },
                                        child: Container(
                                            color: Colors.transparent),
                                      ),
                                    )
                                  : null,
                            ),
                          ),
                          Theme(
                            data: Theme.of(context).copyWith(),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(
                                  10.0, 2.5, (box_height - 5.0), 0.0),
                              child: TextField(
                                maxLength: 25,
                                focusNode: _focusCourtName,
                                autofocus: false,
                                onChanged: (String newValue) {
                                  var hasItem = courtNameList.firstWhere(
                                      (KeyValueModel item) =>
                                          item.key == newValue,
                                      orElse: () => null);

                                  if (hasItem != null) {
                                    setState(() {
                                      courtValue = hasItem.key;
                                      court = hasItem.value;
                                    });
                                  } else {
                                    setState(() {
                                      court = newValue;
                                    });
                                  }
                                  checkEnabled();
                                },
                                controller: _courtController,
                                style: TextStyle(
                                    fontSize: default_font_size,
                                    color: AppColors.black,
                                    fontWeight: FontWeight.w500),
                                decoration: InputDecoration(
                                  counterText: "",
                                  hintText: 'Set court...',
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: ((text_box_height / 2))),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

                Padding(
                  padding: commonPadding(),
                  child: Container(
                    child: customDate("Game date", 10),
                  ),
                ),
                Padding(
                  padding: commonPadding(),
                  child: Container(
                    child: customTime("Starting time", 11),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.fromLTRB(
                      0.0, default_padding, 0.0, (6 * default_padding)),
                  child: Align(
                    child: Container(
                      width: form_width,
                      height: box_height,
                      child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0),
                            side: BorderSide(
                                color: disabledBtnFont,
                                width: 1,
                                style: BorderStyle.solid)),
                        onPressed: () {
                          setState(() {
                            disabledBtn = AppColors.gray;
                            disabledBtnFont = AppColors.white;
                          });

                          if (enableButton == true) {
                            newDataInject();
                          } else {
                            _validationCheck();
                          }
                          // newDataInject();
                        },
                        color: disabledBtn,
                        child: Text(
                          'Save',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: disabledBtnFont,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  //Check the vakidation in button click------------------
  _validationCheck() {
    //match name----------
    var errorStatusMatchName =
        TextBoxValidation.isEmpty(_matchNameController.text);

    if (errorStatusMatchName['state'] == false) {
      setState(() {
        _validateMatchName = errorStatusMatchName['state'];
        messageMatchName = errorStatusMatchName['errorMessage'];
        textBoderColor = Colors.red;
        errorText = errorStatusMatchName['errorMessage'];
      });
    } else {}

//Player A----------------
    var errorStatusPlayerA = TextBoxValidation.isEmpty(_playerAController.text);

    if (errorStatusPlayerA['state'] == false) {
      setState(() {
        _validatePlayerAName = errorStatusPlayerA['state'];
        messagePlayerAName = errorStatusPlayerA['errorMessage'];
        textBoderColorPlayerA = Colors.red;
        errorTextPlayerA = errorStatusPlayerA['errorMessage'];
      });
    } else {}

//Player B------------
    var errorStatusPalyerB = TextBoxValidation.isEmpty(_playerBController.text);

    if (errorStatusPalyerB['state'] == false) {
      setState(() {
        _validatePlayerBName = errorStatusPalyerB['state'];
        messagePlayerBName = errorStatusPalyerB['errorMessage'];
        textBoderColorPlayerB = Colors.red;
        errorTextPlayerB = errorStatusPalyerB['errorMessage'];
      });
    } else {}
  }

  setPlayerList(data) async {
    data.forEach((i) {
      setState(() {
        String pName;
        if (i['Player_no'] < 10) {
          pName = "0" + i['Player_no'].toString() + " - ";
        } else {
          pName = i['Player_no'].toString() + " - ";
        }

        playerNameList.add(
            KeyValueModel(key: (pName + i['name']), value: (i['player_id'])));

        // playerList
        //     .add(KeyValueModel(key: (i['name']), value: (i['player_id'])));

        if (widget.scoreArguments['player1Id'].toString() == i['player_id']) {
          drpDownA = pName + i['name'];
          drpDownAafterChange = pName + i['name'];
          playerANoAfterChange = pName;
          displayPlayerANo = pName;
          _playerAController.text = i['name'];
          playerAControllerValue = i['name'];
        }
        if (widget.scoreArguments['player2Id'].toString() == i['player_id']) {
          drpDownB = pName + i['name'];
          drpDownBafterChange = pName + i['name'];
          playerBNoAfterChange = pName;
          displayPlayerBNo = pName;
          _playerBController.text = i['name'];
          playerBControllerValue = i['name'];
        }
      });
    });
  }

  setStadiumList(data) async {
    data.forEach((i) {
      setState(() {
        stadiumNameList.add(
            KeyValueModel(key: (i['stadium_name']), value: (i['stadium_id'])));
        if (widget.scoreArguments['stadium'].toString() == i['stadium_id']) {
          _stadiumController.text = i['stadium_name'];
          stadiumValue = i['stadium_name'];
        }
      });
    });
    checkEnabled();
  }

  setCourtList(data) async {
    await clearCourtData();

    setState(() {
      data.forEach((i) {
        courtNameList
            .add(KeyValueModel(key: (i['court_name']), value: (i['court_id'])));
        if (tempCourt == i['court_id']) {
          court = tempCourt;
          _courtController.text = i['court_name'];
          courtValue = i['court_name'];
          disabledBtn = AppColors.ternary_color;
          disabledBtnFont = AppColors.black;
          btnEnbled = true;
        }
      });
    });
  }

  clearCourtData() {
    setState(() {
      courtNameList.clear();
    });
  }

  setNameandNo(newValue, player) {
    var text = newValue.key;
    List<String> data = text.split(" - ");

    switch (player) {
      case "A":
        setState(() {
          drpDownA = newValue.key;
          drpDownAafterChange = newValue.key;
          playerANoAfterChange = data[0];
          displayPlayerANo = data[0] + " - ";
          _playerAController.text = data[1];
          playerAControllerValue = data[1];
          player1Id = newValue.value;
        });
        break;

      case "B":
        setState(() {
          drpDownB = newValue.key;
          drpDownBafterChange = newValue.key;
          playerBNoAfterChange = data[0];
          displayPlayerBNo = data[0] + " - ";
          _playerBController.text = data[1];
          playerBControllerValue = data[1];
          player2Id = newValue.value;
        });
        break;
    }
  }

  nameCleared(newValue, player) {
    switch (player) {
      case "A":
        setState(() {
          if (newValue == "") {
            drpDownA = "";
            drpDownAafterChange = "";
            playerANoAfterChange = "";
            displayPlayerANo = "";
            _playerAController.text = "";
            playerAControllerValue = "";
            player1Id = "";
          } else {
            if (newValue != playerAControllerValue) {
              drpDownA = "";
              displayPlayerANo = "";
            } else {
              if (
                  // playerBControllerValue != "" &&
                  newValue != playerBControllerValue) {
                drpDownA = drpDownAafterChange;
                displayPlayerANo = playerANoAfterChange + " - ";
              } else {
                playerAControllerValue = "";
              }
            }
          }
        });
        break;
      case "B":
        setState(() {
          if (newValue == "") {
            drpDownB = "";
            drpDownBafterChange = "";
            playerBNoAfterChange = "";
            displayPlayerBNo = "";
            _playerBController.text = "";
            playerBControllerValue = "";
            player2Id = "";
          } else {
            if (newValue != playerBControllerValue) {
              drpDownB = "";
              displayPlayerBNo = "";
            } else {
              if (
                  // playerAControllerValue != "" &&
                  newValue != playerAControllerValue) {
                drpDownB = drpDownBafterChange;
                displayPlayerBNo = playerBNoAfterChange + " - ";
              } else {
                playerBControllerValue = "";
              }
            }
          }
        });
        break;
      default:
        break;
    }
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            switch (response['calledMethod']) {
              case '_loadPlayerData':
                setPlayerList(response['response_data']);
                break;
              case '_loadStatdiumData':
                setStadiumList(response['response_data']);
                break;
              case '_loadCourtDataDatabase':
                setCourtList(response['response_data']);
                //print("response['response_data']");
                //print(response['response_data']);

                //number of courts in stadium table
                var param2 = [
                  "SELECT no_of_courts FROM stadium  where stadium_id = ?",
                  [stadium],
                  {"calledMethod": 'selectNoFoCourts'}
                ];

                controller.execFunction(ControllerFunc.db_sqlite,
                    ControllerSubFunc.db_select, param2);

                //number of courts registerd
                var param3 = [
                  "SELECT COUNT(*) FROM court  where stadium_id = ? AND status = ?",
                  [stadium, 0],
                  {"calledMethod": 'selectNoFoCourtRegisterd'}
                ];

                controller.execFunction(ControllerFunc.db_sqlite,
                    ControllerSubFunc.db_select, param3);

                break;
              case 'getData':
                break;

              case 'updateData':
                upDateResult();
                break;
              case 'newDataInject':
                updateData();
                break;

              case 'selectPlayerNumber':
                if (response['response_data'].toString() != "null") {
                  if (response['response_data'].toString() != "[]") {
                    int x = response['response_data'][0]['Player_no'] + 1;
                    if (x < 10) {
                      setState(() {
                        playerNo = x;
                      });
                    } else {
                      setState(() {
                        playerNo = x;
                      });
                    }
                  } else {
                    setState(() {
                      playerNo = 1;
                    });
                  }
                }

                break;

              case 'upDateResult':
                widget.gameDataUpdate({
                  'match_id': matchId,
                  'player1Id': player1Id,
                  'player2Id': player2Id,
                  'noOfGames': _noOfGamesController.text.toString(),
                  'noOfSets': noOfSets,
                  'tieBreak': tieBreak,
                  'advantage': advantage,
                  'match_name': _matchNameController.text.toString(),
                  'weather': weather,
                  'stadium': stadium,
                  'court': court,
                  'date': dateObject,
                  'time': timeObject,
                  'temperature': temp,
                });
                widget.toggleStartState(false);
                widget.onSelected(RoutingData.Score, true, false);

                if (courtData == true) {
                  if (noOfCourt == null) {
                    int newCourtCount;
                    newCourtCount = 1;

                    var param = [
                      "UPDATE stadium SET no_of_courts = ?  where stadium_id = ?",
                      [newCourtCount, stadium],
                      {"calledMethod": 'updateStadium'}
                    ];

                    controller.execFunction(ControllerFunc.db_sqlite,
                        ControllerSubFunc.db_update, param);
                  } else {
                    if (noOfCourt == noOfCourtRegisterd) {
                      int newCourtCount;
                      newCourtCount = noOfCourt + 1;

                      var param = [
                        "UPDATE stadium SET no_of_courts = ?  where stadium_id = ?",
                        [newCourtCount, stadium],
                        {"calledMethod": 'updateStadium'}
                      ];

                      controller.execFunction(ControllerFunc.db_sqlite,
                          ControllerSubFunc.db_update, param);
                    } else {}
                  }

                  courtData = false;
                } else {}
                break;

              case 'selectNoFoCourts':
                List tempList = response['response_data'];
                if (tempList.length != 0) {
                  setState(() {
                    noOfCourt = response['response_data'][0]['no_of_courts'];
                  });
                }

                break;

              case 'selectNoFoCourtRegisterd':
                setState(() {
                  noOfCourtRegisterd = response['response_data'][0]['COUNT(*)'];
                });

                break;

              default:
                break;
            }
          } else {
            // print('Data Base Error');
          }
        }
        break;
      default:
        {
          // print('NOT SQLITE METHOD');
        }
    }
  }
}

//Create a Model class to hold key-value pair data
class KeyValueModel {
  String key;
  String value;

  KeyValueModel({this.key, this.value});
}
