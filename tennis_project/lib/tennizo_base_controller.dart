import 'dart:io';

import 'package:tenizo/controller/app_launcher.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:tenizo/database/tennizo_db.dart';

class BaseController with SQLiteControllerListener, AppLauncherListener {
  BaseControllerListner _listner;
  SQLiteController _sqlLiteDB;
  AppLauncher _app_launcher;

  BaseController(BaseControllerListner _listner) {
    this._listner = _listner;
    _sqlLiteDB = new SQLiteController(this);
    _app_launcher = new AppLauncher(this);
  }

  //Execute controller functions
  void execFunction(func, subFunc, param) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (subFunc == ControllerSubFunc.db_select) {
            _sqlLiteDB.execQuery(subFunc, param[0], param);
          } else if (subFunc == ControllerSubFunc.db_insert) {
            _sqlLiteDB.execQuery(subFunc, param[0], param);
          } else if (subFunc == ControllerSubFunc.db_update) {
            _sqlLiteDB.execQuery(subFunc, param[0], param);
          } else if (subFunc == ControllerSubFunc.db_delete) {
            _sqlLiteDB.execQuery(subFunc, param[0], param);
          } else if (subFunc == ControllerSubFunc.db_select_batch) {
            _sqlLiteDB.execQuery(subFunc, param[0], param);
          }
          break;
        }
      case ControllerFunc.launch_app:
        {
          if (subFunc == ControllerSubFunc.launch_mailer ||
              subFunc == ControllerSubFunc.launch_map) {
            _app_launcher = new AppLauncher(this);
            _app_launcher.runApp(subFunc, param);
          }
          break;
        }

      default:
        {
          // do nothing
        }
    }
  }

  @override
  sqlLiteResponse(subFunc, param) {
    _listner.resultFunction(ControllerFunc.db_sqlite, subFunc, param);
  }

  @override
  setAppLauncherStatus(subFunc, param) {
    _listner.resultFunction(ControllerFunc.launch_app, subFunc, param);
  }
}

abstract class BaseControllerListner {
  resultFunction(func, subFunc, response);
}
