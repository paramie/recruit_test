import 'package:flutter/material.dart';

class AppTerms {
  // TERMS
  static const String projectName = "TenniZo";
  static const String projectDescription = "Tennis Score Management System";
}

class UserDetails{
  static String username = "";
}

class AppStyles {
  // FONT SIZES
  static const xultralarge_font = 36.0;
  static const ultralarge_font = 34.0;
  static const xxxlarge_font = 30.0;
  static const xxlarge_font = 26.0;
  static const xlarge_font = 24.0;
  static const large_font = 20.0;
  static const normal_font = 18.0;
  static const small_font = 16.0;
  static const tiny_font = 14.0;
}

class AppColors {
  //MAIN COLORS
  static const primary_color = const Color(0xFF00D9D9);
  static const secondary_color = const Color(0xFFE5F9E0);
  static const ternary_color = const Color(0xFFFBE4BF);
  static const black = const Color(0xFF000000);
  static const white = const Color(0xFFFFFFFF);
  static const gray = const Color(0xFFcccccc);
  static const lightGray = const Color(0xFFEDEDED);
  static const backgroundColor = const Color(0xFFFAFAFA);

  //SPLASH SCREEN
  static const splash_font = const Color(0xFF016868);
  static const form_border = const Color(0xFF808080);
}

class ScoreValues {
  static const ace = 'ACE';
  static const winner = 'WINNER';
  static const uErr = 'uERR';
  static const fErr = 'fERR';
  static const fault = 'FAULT';
  static String flt = 'FAULT';
  static String dflt = 'D.FAULT';
  static String tieBreak = 'Tie Break';
  static String noTieBreak = 'No Tie Break';
  static String superTieBreak = 'Super tie break';
  static String advantage = 'Advantage';
  static String noAdvantage = 'No Advantage';
  static String semiAdvantage = 'Semi Advantage';
  static String pause = 'pause';
  static String cancel = 'cancel';
  static String stop = 'stop';
  static String delete = 'delete';
  static String backHand = 'BACKHAND';
  static String backHandVolley = 'BACKHAND VOLLEY';
  static String foreHand = 'FOREHAND';
  static String forHandVolley = 'FOREHAND VOLLEY';
  static String smash = 'Smash';
  
  static String uErrBackHand = 'uERR BACKHAND';
  static String uErrBackHandVolley = 'uERR BACKHAND VOLLEY';
  static String uErrForeHand = 'uERR FOREHAND';
  static String uErrForHandVolley = 'uERR FOREHAND VOLLEY';
  static String uErrSmash = 'uERR Smash';

  static String fErrBackHand = 'fERR BACKHAND';
  static String fErrBackHandVolley = 'fERR BACKHAND VOLLEY';
  static String fErrForeHand = 'fERR FOREHAND';
  static String fErrForHandVolley = 'fERR FOREHAND VOLLEY';
  static String fErrSmash = 'fERR Smash';
}

class CommonValues {
  static String unknown = 'Unknown';
  static String rightHanded = 'Right handed';
  static String leftHanded = 'Left handed';
  static String doubleHanded = 'Double handed';
  static String baseline = 'Baseline';
  static String attackTheNet = 'Attack the Net';
  static String counterPunching = 'Counter Punching';
  static String allCourt = 'All Court';
  static String roleOther = 'Other';
  static String roleKey = 'Key';
  static String genderMale = 'Male';
  static String genderFemale = 'Female';
  //Court
  static String surfaceClay = 'Clay';
  static String surfaceHard = 'Hard';
  static String surfaceGrass = 'Grass';
  static String surfaceCarpet = 'Carpet';
  static String indoor = 'Indoor';
  static String outdoor = 'Outrdoor';
  static String notSet = 'Not set';
  //weather
  static String clear = 'Clear';
  static String cloudy = 'Cloudy';
  static String rainy = 'Rainy';
  static String snow = 'Snow';

}

enum RoutingData {
  Players,
  Stadium,
  Score,
  Home,
  GameSettings,
  StadiumRegistration,
  PlayerRegistration,
  PlayerProfile,
  StadiumDetails,
  StadiumEdit,
  PlayerEdit,
  CourtRegistration,
  CourtView,
  CourtEdit,
  Stats,
  GameSettingUpdate,
  EditUser,
  Flow,
  HelpPage,
  ContactUs,

}
