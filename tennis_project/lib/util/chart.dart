// import 'dart:async';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';

// class MiddleMyPainter extends StatefulWidget {
//   // final double progress;
//   final double sheetWidthFactor;
//   final double sheetHightFactor;
//   final double dataSpreadXFactor;
//   final double dataSpreadYFactor;
//   final double scaler;
//   final double dataSpreadFactorA;
//   final double dataSpreadFactorB;
//   final double graphInnerBoxXoffsetFactorLeft;
//   final double graphInnerBoxXoffsetFactorRight;
//   final double graphInnerBoxYoffsetFactorTop;
//   final double graphInnerBoxYoffsetFactorBottom;
//   final double finalWidthOFSHEET;
//   final double finalHightOFSHEET;
//   final double focusPosX;
//   final double focusPosY;
//   final Function setArrowPositionOnTheFly;
//   final double testPosX;
//   final double testPosY;
//   final double dotSize;
//   final List<Pointer> scoreValues;

//   @override
//   _MiddleMyPainter createState() => _MiddleMyPainter();
//   MiddleMyPainter(
//     Key key,
//     this.sheetWidthFactor,
//     this.sheetHightFactor,
//     this.dataSpreadXFactor,
//     this.dataSpreadYFactor,
//     this.scaler,
//     this.dataSpreadFactorA,
//     this.dataSpreadFactorB,
//     this.graphInnerBoxXoffsetFactorLeft,
//     this.graphInnerBoxXoffsetFactorRight,
//     this.graphInnerBoxYoffsetFactorTop,
//     this.graphInnerBoxYoffsetFactorBottom,
//     this.finalWidthOFSHEET,
//     this.finalHightOFSHEET,
//     this.focusPosX,
//     this.focusPosY,
//     this.setArrowPositionOnTheFly,
//     this.testPosX,
//     this.testPosY,
//     this.dotSize,
//     this.scoreValues,
//   ) : super(key: key);
// }

// class _MiddleMyPainter extends State<MiddleMyPainter> {
//   @override
//   Widget build(BuildContext context) {
//     return null;
//   }
// }

class MyPainter extends CustomPainter {
  // final double progress;
  double sheetWidthFactor;
  double sheetHightFactor;
  double dataSpreadXFactor;
  double dataSpreadYFactor;
  double scaler;
  double dataSpreadFactorA;
  double dataSpreadFactorB;
  double graphInnerBoxXoffsetFactorLeft;
  double graphInnerBoxXoffsetFactorRight;
  double graphInnerBoxYoffsetFactorTop;
  double graphInnerBoxYoffsetFactorBottom;
  double finalWidthOFSHEET;
  double finalHightOFSHEET;
  double focusPosX;
  double focusPosY;
  Function setArrowPositionOnTheFly;
  double testPosX;
  double testPosY;
  double dotSize;
  List<Pointer> scoreValues;

  MyPainter(
    this.sheetWidthFactor,
    this.sheetHightFactor,
    this.dataSpreadXFactor,
    this.dataSpreadYFactor,
    this.scaler,
    this.dataSpreadFactorA,
    this.dataSpreadFactorB,
    this.graphInnerBoxXoffsetFactorLeft,
    this.graphInnerBoxXoffsetFactorRight,
    this.graphInnerBoxYoffsetFactorTop,
    this.graphInnerBoxYoffsetFactorBottom,
    this.finalWidthOFSHEET,
    this.finalHightOFSHEET,
    this.focusPosX,
    this.focusPosY,
    this.setArrowPositionOnTheFly,
    this.testPosX,
    this.testPosY,
    this.dotSize,
    this.scoreValues,
  );

  @override
  void paint(Canvas canvas, Size size) {
    // Graph Box Drawing -------------------------------------------
    // this.scaler =10;
    double graphInnerBoxWidth = scaler * dataSpreadXFactor;
    double graphInnerBoxHeight = scaler * dataSpreadYFactor;
    double graphInnerBoxXLeftOffset = scaler * graphInnerBoxXoffsetFactorLeft;
    double graphInnerBoxYTopOffset = scaler * graphInnerBoxYoffsetFactorTop;

    double xAxisStartXFactor = graphInnerBoxXoffsetFactorLeft;
    double xAxisStartYFactor =
        graphInnerBoxYoffsetFactorTop + dataSpreadFactorA;
    double xAxisEndXFactor = graphInnerBoxXoffsetFactorLeft + dataSpreadXFactor;
    double xAxisEndYFactor = xAxisStartYFactor;

    double yAxisStartXFactor = graphInnerBoxXoffsetFactorLeft;
    double yAxisStartYFactor = graphInnerBoxYoffsetFactorTop;
    double yAxisEndXFactor = yAxisStartXFactor;
    double yAxisEndYFactor = graphInnerBoxYoffsetFactorTop + dataSpreadYFactor;

    // double centerX = finalWidthOFSHEET / 2;
    // double centerY = finalHightOFSHEET / 2;
    // double dotSize = 18;
    double typeDotSize = dotSize/3;
    double gapOffset = typeDotSize/3;
    double typeDotSizeOffsetFactor = dotSize / 2 + typeDotSize / 2 + gapOffset;

    // ******************************************************************
    // ************************* UTILITIES ******************************
    var paragraphBuilder;
    var paragraph;

    double fixSize(val) {
      return val * scaler;
    }

    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    double fixYOffSet(val) {
      return fixSize(xAxisStartYFactor - val);
    }

    double fixXOffSet(val) {
      return fixSize(xAxisStartXFactor + val);
    }

    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    void drawAText(
      double textWidth,
      double textHeight,
      Color textColor,
      textVal,
      double xVal,
      double yVal,
      TextAlign align,
      TextDirection txtDirection,
    ) {
      // Initialize text
      paragraphBuilder = ui.ParagraphBuilder(
        ui.ParagraphStyle(textDirection: txtDirection, textAlign: align),
      )
        ..pushStyle(
          ui.TextStyle(
            color: textColor,
            fontSize: textHeight,
          ),
        )
        ..addText(textVal);
      // Build text
      paragraph = paragraphBuilder.build();
      paragraph.layout(ui.ParagraphConstraints(width: textWidth));
      // Draw text
      canvas.drawParagraph(
        paragraph,
        Offset(
          xVal,
          yVal,
        ),
      );
    }

    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    void drawARectangle(
      double left,
      double top,
      double width,
      double height,
      Color paintColor,
      double strokeWidth,
      PaintingStyle paintStyle,
    ) {
      canvas.drawRect(
        Rect.fromLTWH(left, top, width, height),
        Paint()
          ..color = paintColor
          ..strokeWidth = strokeWidth
          ..style = paintStyle,
      );
    }

    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    void drawACircle(
      double xVal,
      double yVal,
      double radius,
      Color paintColor,
      double strokeWidth,
      PaintingStyle paintStyle,
    ) {
      canvas.drawCircle(
        Offset(xVal, yVal),
        radius,
        Paint()
          ..color = paintColor
          ..strokeWidth = strokeWidth
          ..style = paintStyle,
      );
    }

    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    void drawApoint(
      double xVal,
      double yVal,
      Color paintColor,
      double strokeWidth,
      StrokeCap capStyle,
    ) {
      canvas.drawPoints(
        ui.PointMode.points,
        [Offset(xVal, yVal)],
        Paint()
          ..color = paintColor
          ..strokeWidth = strokeWidth
          ..strokeCap = capStyle,
      );
    }

    // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    void drawAline(
      Offset start,
      Offset end,
      Color paintColor,
      double strokeWidth,
      StrokeCap capStyle,
    ) {
      canvas.drawLine(
        start,
        end,
        Paint()
          ..color = paintColor
          ..strokeWidth = strokeWidth
          ..strokeCap = capStyle,
      );
    }
    // ********************** UTILITIES DONE ****************************
    // ******************************************************************

    // FUNCTION -> DROW Rally NUMBERS +++++++++++++++++++++++++++++++++++
    void drowATextForRallyCount(txtVal, xVal, yVal) {
      double fontHight = 14;
      double fontWidth = 30;
      drawAText(
        fontWidth,
        fontHight,
        Colors.white,
        txtVal.toString(),
        xVal - (fontWidth / 2.0),
        yVal - (fontHight / 1.8),
        TextAlign.center,
        TextDirection.ltr,
      );
    }

    // FUNCTION -> DRAW THE TYPE DOT ++++++++++++++++++++++++++++++++++
    void drowTypeDot(type, xVal, yVal) {
      Color pointClr = Colors.white;
      switch (type) {
        case 1:
          pointClr = Color(0xFFDB2828);
          // Aces - red
          break;
        case 2:
          pointClr = Color(0xFFFBBD08);
          // Double Fault - blue
          break;
        case 3:
          pointClr = Color(0xFF00B5AD);
          // Winner
          break;
        case 4:
          pointClr = Color(0xFF00B5AD);
          // Winner - purple BackHand
          break;
        case 5:
          pointClr = Color(0xFF00B5AD);
          // Winner -orange BackHandVolley
          break;
        case 6:
          pointClr = Color(0xFF00B5AD);
          // Winner -orange ForeHand
          break;
        case 7:
          pointClr = Color(0xFF00B5AD);
          // Winner -orange ForeHandVolley
          break;
        case 8:
          pointClr = Color(0xFF00B5AD);
          // Winner -orange Smash
          break;
        case 9:
          pointClr = Color(0xFF000000);
          // Unforced Error
          break;
        case 10:
          pointClr = Color(0xFF000000);
          // Unforced Error -uErrorBackHand
          break;
        case 11:
          pointClr = Color(0xFF000000);
          // Unforced Error -uErrorBackHandVolley
          break;
        case 12:
          pointClr = Color(0xFF000000);
          // Unforced Error -uErrForeHand
          break;
        case 13:
          pointClr = Color(0xFF000000);
          // Unforced Error -uErrForeHandVolley
          break;
        case 14:
          pointClr = Color(0xFF000000);
          // Unforced Error -uErrSmash
          break;
        case 15:
          pointClr = Color(0xFF0931FC);
          // f.Error - teal
          break;
        case 16:
          pointClr = Color(0xFF0931FC);
          // Unforced Error fErrBackHand
          break;
        case 17:
          pointClr = Color(0xFF0931FC);
          // Unforced Error fErrBackHandVolley
          break;
        case 18:
          pointClr = Color(0xFF0931FC);
          // Unforced Error fErrForHand
          break;
        case 19:
          pointClr = Color(0xFF0931FC);
          // Unforced Error fErrForeHandVolley
          break;
        case 20:
          pointClr = Color(0xFF0931FC);
          // Unforced Error fErrSmash
          break;
        default:
          pointClr = Color(0xFFFFFFFF);
      }

      drawACircle(
        xVal,
        yVal - typeDotSizeOffsetFactor,
        typeDotSize / 2,
        pointClr,
        0,
        PaintingStyle.fill,
      );
    }

    void toggleServeReserve(
      String p1,
      String p2,
      double fontWidth,
      double fontHight,
      double xVal,
      bool winner,
    ) {
      drawAText(
        fontWidth,
        fontHight,
        Colors.black,
        p1,
        xVal,
        fixSize(graphInnerBoxYoffsetFactorTop - 1) - (fontHight / 1.8),
        TextAlign.center,
        TextDirection.ltr,
      );

      drawAText(
        fontWidth,
        fontHight,
        Colors.black,
        p2,
        xVal,
        fixSize(dataSpreadYFactor + graphInnerBoxYoffsetFactorTop + 1) -
            (fontHight / 1.8),
        TextAlign.center,
        TextDirection.ltr,
      );

      drawACircle(
        xVal + fontWidth / 2,
        winner
            ? fixSize(graphInnerBoxYoffsetFactorTop - 1)
            : fixSize(dataSpreadYFactor + graphInnerBoxYoffsetFactorTop + 1),
        fontHight * .64,
        Colors.black,
        2.0,
        PaintingStyle.stroke,
      );
    }

    // FUNCTION -> DRAW SERVE AND SET BOX +++++++++++++++++++++++++++++
    void drawServe(
      bool order,
      double xVal,
      double extraOffset,
      int player1Games,
      int player2Games,
      int gameWinner,
      int setCount,
      int setFinished,
    ) {
      //Initialize Values
      double fontHight = 18;
      double fontWidth = 32;
      double squareWidth = fixSize(3.5);
      double squareHeight = fixSize(1);
      double xCalculation = 0;

      if (extraOffset > 0) {
        xCalculation =
            fixSize(xAxisStartXFactor + (xVal + extraOffset + 1) / 2);
      } else {
        xCalculation =
            fixSize(xAxisStartXFactor + (xVal + extraOffset + .5) / 2);
      }

      if (order) {
        toggleServeReserve(
          "S",
          "R",
          fontWidth,
          fontHight,
          xCalculation - (fontWidth / 2.0),
          gameWinner > 0 ? true : false,
        );
      } else {
        toggleServeReserve(
          "R",
          "S",
          fontWidth,
          fontHight,
          xCalculation - (fontWidth / 2.0),
          gameWinner > 0 ? true : false,
        );
      }

      drawARectangle(
        xCalculation - squareWidth / 2,
        fixSize(.25),
        squareWidth,
        squareHeight,
        Colors.black,
        .5,
        PaintingStyle.stroke,
      );

      drawAText(
        fontWidth,
        fontHight,
        Colors.black,
        player1Games.toString() + "-" + player2Games.toString(),
        xCalculation - (fontWidth / 2.0),
        fixSize(.25) + squareHeight / 2 - (fontHight / 1.8),
        TextAlign.right,
        TextDirection.ltr,
      );

      drawAText(
        fontWidth,
        fontHight,
        Colors.red,
        setCount.toString(),
        xCalculation - (fontWidth),
        fixSize(.25) + squareHeight / 2 - (fontHight / 1.8),
        TextAlign.left,
        TextDirection.ltr,
      );
    }

    // FUNCTION -> DRAW THE SET LINE +++++++++++++++++++++++++++++
    void drawSetLine(xVal, int setFinished) {
      if (setFinished > 0) {
        drawAline(
          Offset(
            fixSize(xAxisStartXFactor + xVal + .5),
            fixSize(yAxisStartYFactor),
          ),
          Offset(
            fixSize(xAxisStartXFactor + xVal + .5),
            fixSize(yAxisEndYFactor),
          ),
          Colors.red,
          1.0,
          StrokeCap.round,
        );
      } else {
        drawAline(
          Offset(
            fixSize(xAxisStartXFactor + xVal + .5),
            fixSize(yAxisStartYFactor),
          ),
          Offset(
            fixSize(xAxisStartXFactor + xVal + .5),
            fixSize(yAxisEndYFactor),
          ),
          Colors.blue,
          1.0,
          StrokeCap.round,
        );
      }
    }

    // FUNCTION -> DRAW THE CARTESIAN PLANE +++++++++++++++++++++++++++++
    void drawCartesianPlane() {
      // Draw the Background Rectangle
      drawARectangle(
        graphInnerBoxXLeftOffset,
        graphInnerBoxYTopOffset,
        graphInnerBoxWidth,
        graphInnerBoxHeight,
        Colors.white,
        2.0,
        PaintingStyle.fill,
      );
      // Draw the horizontal lines
      for (double i = graphInnerBoxYoffsetFactorTop;
          i <= yAxisEndYFactor;
          i++) {
        drawAline(
          Offset(graphInnerBoxXLeftOffset, scaler * i),
          Offset(xAxisEndXFactor * scaler, scaler * i),
          Colors.grey,
          0.5,
          StrokeCap.round,
        );
      }
      // Draw the vertical lines
      for (double i = graphInnerBoxXoffsetFactorLeft;
          i <= xAxisEndXFactor;
          i++) {
        drawAline(
          Offset(scaler * i, graphInnerBoxYTopOffset),
          Offset(scaler * i, yAxisEndYFactor * scaler),
          Colors.grey,
          0.5,
          StrokeCap.round,
        );
      }

      // Draw the x axis
      drawAline(
        Offset(fixSize(xAxisStartXFactor), fixSize(xAxisStartYFactor)),
        Offset(fixSize(xAxisEndXFactor), fixSize(xAxisEndYFactor)),
        Colors.black,
        2,
        StrokeCap.round,
      );

      // Draw the y axis
      drawAline(
        Offset(fixSize(yAxisStartXFactor), fixSize(yAxisStartYFactor)),
        Offset(fixSize(yAxisEndXFactor), fixSize(yAxisEndYFactor)),
        Colors.black,
        2,
        StrokeCap.round,
      );
    }

    void drowDescription() {
      double textWidth = 200;
      double textHeight = 18;
      double circleWidth = fixSize(.4);
      double textCircleGap = 10;
      double gap = 1;
      double row = 0;
      double rowGap = 1.5;
      double firstColumn = 2;
      double secondColumn = 10;

      final desData = [
        Describer(Color(0xFFDB2828), "Aces"),
        Describer(Color(0xFFA333C8), "First Services"),
        Describer(Color(0xFFFBBD08), "Double Fault"),
        Describer(Color(0xFFF2711C), "Second Service"),
        Describer(Color(0xFF0931FC), "Forced Error"),
        Describer(Color(0xFF000000), "Unforced Error"),
        Describer(Color(0xFF00B5AD), "Winner"),
      ];

      for (var i = 0; i < desData.length; i++) {
        if (i % 2 == 0) {
          row += rowGap;
          drawACircle(
            fixSize(firstColumn),
            fixSize(
                dataSpreadYFactor + graphInnerBoxYoffsetFactorTop + gap + row),
            circleWidth,
            desData[i].desColor,
            2.0,
            PaintingStyle.fill,
          );

          drawAText(
            textWidth,
            18,
            Colors.black,
            desData[i].desText,
            fixSize(firstColumn) + circleWidth + textCircleGap,
            fixSize(dataSpreadYFactor +
                    graphInnerBoxYoffsetFactorTop +
                    gap +
                    row) -
                textHeight / 1.8,
            TextAlign.left,
            TextDirection.ltr,
          );
        } else {
          drawACircle(
            fixSize(secondColumn),
            fixSize(
                dataSpreadYFactor + graphInnerBoxYoffsetFactorTop + gap + row),
            circleWidth,
            desData[i].desColor,
            2.0,
            PaintingStyle.fill,
          );

          drawAText(
            textWidth,
            18,
            Colors.black,
            desData[i].desText,
            fixSize(secondColumn) + circleWidth + textCircleGap,
            fixSize(dataSpreadYFactor +
                    graphInnerBoxYoffsetFactorTop +
                    gap +
                    row) -
                textHeight / 1.8,
            TextAlign.left,
            TextDirection.ltr,
          );
        }
      }
    }

    // DRAW THE SET LINE _________________________________________________________

    drawCartesianPlane();
    //drowDescription();

    // drawAText(
    //   200,
    //   18,
    //   Colors.black,
    //   "Sachin Kodagoda",
    //   fixSize(1),
    //   fixSize(dataSpreadYFactor / 2 + graphInnerBoxYoffsetFactorTop),
    //   TextAlign.right,
    //   TextDirection.ltr,
    // );

    final graphPaint = new Paint()
      ..color = Colors.black
      ..strokeWidth = 1
      ..strokeCap = StrokeCap.round;

    final aPointPaint = new Paint()
      ..color = Color(0xFFA333C8)
      ..strokeWidth = dotSize
      ..strokeCap = StrokeCap.round;

    final bPointPaint = new Paint()
      ..color = Color(0xFFF2711C)
      ..strokeWidth = dotSize
      ..strokeCap = StrokeCap.round;

    List<Offset> aDots = new List();
    List<Offset> bDots = new List();
    List<Offset> graphPoints = new List();

    int start = 1;
    double pos = 0;
    double setMiddle = 0;
    double trueCenter = 0;
    graphPoints.add(Offset(fixXOffSet(0), fixYOffSet(0)));

    for (int i = 0; i < scoreValues.length; i++) {
      pos += scoreValues[i].pointWinner;
      graphPoints.add(Offset(fixXOffSet(start), fixYOffSet(pos)));
      scoreValues[i].yVal = pos;
      scoreValues[i].xVal = start.toDouble();
      scoreValues[i].xTrueVal = fixXOffSet(start);
      scoreValues[i].yTrueVal = fixYOffSet(pos);
      // Set the dots
      if (scoreValues[i].serviceChance == 1) {
        aDots.add(Offset(fixXOffSet(start), fixYOffSet(pos)));
      } else {
        bDots.add(Offset(fixXOffSet(start), fixYOffSet(pos)));
      }

      drowTypeDot(scoreValues[i].type, fixXOffSet(start), fixYOffSet(pos));
      if (scoreValues[i].gameFinished == 1) {
        drawSetLine(start, scoreValues[i].setFinished);
        drawServe(
          scoreValues[i].serve > 0 ? true : false,
          start.toDouble(),
          setMiddle,
          scoreValues[i].player1Games,
          scoreValues[i].player2Games,
          scoreValues[i].gameWinner,
          scoreValues[i].setCount,
          scoreValues[i].setFinished,
        );
        setMiddle = start.toDouble();
      }
      start++;
    }
    canvas.drawPoints(ui.PointMode.polygon, graphPoints, graphPaint);
    canvas.drawPoints(ui.PointMode.points, aDots, aPointPaint);
    canvas.drawPoints(ui.PointMode.points, bDots, bPointPaint);
    for (int i = 0; i < scoreValues.length; i++) {
      drowATextForRallyCount(scoreValues[i].rallyCount,
          fixXOffSet(scoreValues[i].xVal), fixYOffSet(scoreValues[i].yVal));
    }
    // drawACircle(
    //   finalWidthOFSHEET / 2,
    //   finalHightOFSHEET / 2,
    //   5,
    //   Colors.black,
    //   5.0,
    //   PaintingStyle.fill,
    // );

    // drawACircle(
    //   testPosX,
    //   testPosY,
    //   5,
    //   Colors.blue,
    //   2.0,
    //   PaintingStyle.fill,
    // );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // return oldDelegate.progress != progress;
    return false;
  }
}

class Describer {
  Color desColor;
  String desText;
  Describer(
    this.desColor,
    this.desText,
  );
}

class Pointer {
  int id;
  double xTrueVal;
  double yTrueVal;
  int rallyCount;
  double xVal;
  double yVal;
  int pointWinner;
  int type;
  int gameFinished;
  int serve;
  int player1Games;
  int player2Games;
  int gameWinner;
  int setCount;
  int setFinished;
  int serviceChance;
  Pointer(
    this.xTrueVal,
    this.yTrueVal,
    this.xVal,
    this.yVal,
    this.rallyCount,
    this.pointWinner,
    this.type,
    this.gameFinished,
    this.serve,
    this.player1Games,
    this.player2Games,
    this.gameWinner,
    this.setCount,
    this.setFinished,
    this.serviceChance,
    this.id,
  );
}
