import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/util/app_const.dart';

class CustomAppPopUpForDelete extends StatelessWidget {
  final BuildContext context;
  final String optionType;
  final List<String> param;
  final Function callBackFunction;
  CustomAppPopUpForDelete(
      this.context, this.optionType, this.param, this.callBackFunction);

  @override
  Widget build(BuildContext context) {
    String firstButtonText = '';
    String secondButtonText = 'Cancel';
    String topicText = '';
    var textDesplay = "";
    var iconDesplay;
    bool popup;

    if (optionType == "Delete") {
      textDesplay = 'Deleted';
      topicText = 'Delete';
      firstButtonText = "OK";
      iconDesplay = Icons.delete;
      popup = false;
    }
    //for court and stadium delete
    else if (optionType == "CSDelete") {
      textDesplay = 'Deleted';
      topicText = 'Delete';
      firstButtonText = "OK";
      iconDesplay = Icons.delete;
      popup = true;
    }

    return AlertDialog(
      title: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Icon(iconDesplay, size: 30, color: Colors.black),
              ],
            ),
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: new Text(
                    topicText,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 25,
                        color: Colors.black,
                        fontStyle: FontStyle.normal),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      content: new Text(
          "This Content will be " + textDesplay + ". Do you want to continue ?",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
              color: Colors.black,
              fontStyle: FontStyle.normal)),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 35.0, bottom: 10.0),
          child: Container(
            child: Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(
                      width: 100,
                      child: new RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            side: BorderSide(
                                color: AppStyle.color_Head,
                                style: BorderStyle.solid)),
                        color: AppStyle.color_Head,
                        child: new Text(
                          firstButtonText,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              color: Colors.white,
                              fontStyle: FontStyle.normal),
                        ),
                        onPressed: () => {
                          // Navigator.of(context).pop(),
                          callBackFunction(param)
                        },
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: SizedBox(
                        width: 100,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                              side: BorderSide(
                                  color: AppStyle.color_Head,
                                  style: BorderStyle.solid)),
                          color: AppStyle.color_Head,
                          child: new Text(
                            secondButtonText,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white,
                                fontStyle: FontStyle.normal),
                          ),
                          onPressed: () => {
                            if (popup)
                              {
                                Navigator.of(context).pop(),
                                callBackFunction(["cancel", optionType])
                              }
                            else
                              {
                                callBackFunction(["cancel", optionType])
                              }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
